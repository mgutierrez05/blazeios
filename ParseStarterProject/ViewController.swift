//
//  ViewController.swift
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

import UIKit
import Parse
import CoreLocation

class ViewController: UIViewController{
    
    
    @IBOutlet weak var textfb: UILabel!
    @IBOutlet weak var mainLogo: UIImageView!
    @IBOutlet weak var fbBtn: UIButton!
    
    let permissions = ["email","user_birthday", "user_friends","user_photos"]
//    var locationManager: CLLocationManager!
    var manager: OneShotLocationManager?
    var seenError: Bool = false
    var locationFixArchive: Bool = false
    var locationStatus: NSString = NSLocalizedString("Not Started", comment: "")
    var coordinate: CLLocationCoordinate2D!
    var clickedLoagin:Bool = false
    
    var logoYOrigin:CGFloat = 0
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUIConfiguration()
    }

    @IBAction func makeLogOut(sender: AnyObject) {
        PFFacebookUtils.session()?.closeAndClearTokenInformation()
        PFUser.logOut()
    }
    @IBAction func makeLoginWithFB(sender: AnyObject) {
        
        if !clickedLoagin {
            JLToast.makeText(NSLocalizedString("Cargando...", comment: "")).show()
            clickedLoagin = true
            loadLocationManager()
        }
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadUIConfiguration() {
        let color:UIColor = UIColor(red: 245/255, green: 219/255, blue: 50/255, alpha: 1)
        view.backgroundColor = color
        navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: false)
        //mainLogo.frame = LogoRectMake(Double(view.frame.size.width),Double(view.frame.size.height))
        //fbBtn.frame = FBBtnRectMake(Double(view.frame.size.width),Double(view.frame.size.height))
        textfb.hidden = true
        fbBtn.hidden = true
        //logoYOrigin = mainLogo.frame.origin.y
        //mainLogo.frame.origin.y = (view.frame.size.height / 2 ) - (mainLogo.frame.height / 2 )
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadInfo()
    }
    func loadInfo(){
        if let session = PFFacebookUtils.session() {
            if session.isOpen {
                println("session is open")
//                FBRequestConnection.startForMeWithCompletionHandler({ (connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
//                    println("done me request")
//                    if error != nil {
//                        println("facebook me request - error is not nil :(")
//                    } else {
//                        println("facebook me request - error is nil :)")
//                        println(result)
//                        // You have 2 ways to access the result:
//                        // 1)
//                        println(result["name"])
//                        println(result["id"])
//                        // 2)
//                        println(result.name)
//                        println(result.objectID)
//                        // Save to Parse:
//                        
//                    }
//                })
                
                loadLocationManagerInit()
//                self.navigationController?.setNavigationBarHidden(self.navigationController?.navigationBarHidden == false, animated: false)
//                self.performSegueWithIdentifier("goToLoading", sender: self)
            }else{
                
                 println("is not open")
                UIView.animateWithDuration(0.25, animations: {
         
                    self.textfb.hidden = true
                    self.fbBtn.hidden = true
                    self.mainLogo.frame.origin.y = self.logoYOrigin
                    
                })
            }
        }else{
            println("is not open")
            UIView.animateWithDuration(0.25, animations: {
   
                self.textfb.hidden = false
                self.fbBtn.hidden = false
                self.mainLogo.frame.origin.y = self.logoYOrigin
                
            })
        }
    }
    func loadData(){
        let request:FBRequest = FBRequest.requestForMe()
        request.startWithCompletionHandler { (connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            if error == nil{
                if let dict = result as? Dictionary<String, AnyObject>{
                    let name:String = dict["name"] as AnyObject? as! String
                    let facebookID:String = dict["id"] as AnyObject? as! String
                    
                    let gender:String = dict["gender"] != nil ? dict["gender"] as AnyObject? as! String : ""
                    
                    var birthday:NSDate = NSDate()
                    let formatter =  NSDateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy"
                    
                    if var user_birthday = dict["birthday"] as? String {
                        birthday = formatter.dateFromString(user_birthday)!
                    }
                    
                    let pictureURL = "https://graph.facebook.com/\(facebookID)/picture?type=large&return_ssl_resources=1"
                    
                    var URLRequest = NSURL(string: pictureURL)
                    var URLRequestNeeded = NSURLRequest(URL: URLRequest!)
                    
                    
                    NSURLConnection.sendAsynchronousRequest(URLRequestNeeded, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!, error: NSError!) -> Void in
                        if error == nil {
                            var picture = PFFile(data: data)
                            PFUser.currentUser()!.setObject(picture, forKey: "profilePicture")
                            PFUser.currentUser()!.saveInBackground()
                        }
                        else {
                            println("Error: \(error.localizedDescription)")
                            JLToast.makeText("Error: \(error.localizedDescription)").show()

                        }
                        JLToast.makeText(NSLocalizedString("Guardando perfil", comment: "")).show()
                        let point = PFGeoPoint(latitude:self.coordinate.latitude, longitude:self.coordinate.longitude)
                        var fullNameArr = split(name) {$0 == " "}
                        PFUser.currentUser()!.setValue(point, forKey: "location")
                        PFUser.currentUser()!.setValue(fullNameArr[0], forKey: "username")
                        PFUser.currentUser()!.setValue(gender, forKey: "gender")
                        PFUser.currentUser()!.setValue(false, forKey: "isPlus")
                        PFUser.currentUser()?.setValue(facebookID, forKey: "id_facebook")
                        PFUser.currentUser()!.setValue(birthday		, forKey: "birthday")
                        if let email:String = dict["email"] as? String {
                             PFUser.currentUser()!.setValue(email, forKey: "email")
                        }
                        
                        var myPreferences:PFObject = PFObject(className: "preferences")
                        var groupACL = PFACL()
//                        groupACL.setReadAccess(true, forUser:self.senderUser)
//                        groupACL.setWriteAccess(true, forUser:self.senderUser)
//                                                groupACL.setReadAccess(true, forUser:self.addresseeUser)
//                                                groupACL.setWriteAccess(true, forUser:self.addresseeUser)
                        // 1 Hombres
                        // 2 Mujeres
                        var genderNumber = 1
                        var genderString = "male"
                        if gender == "male" {
                            genderNumber = 2
                            genderString = "female"
                        }
                        
                        //myFriendship.ACL = groupACL
                        //println(PFUser.currentUser()?.valueForKey("objectId"))
                        myPreferences["userId"] = PFUser.currentUser()?.valueForKey("objectId")
                        myPreferences["found_me"] = true
                        myPreferences["distance"] = 2.5
                        myPreferences["age_min"] = 18
                        myPreferences["age_max"] = 55
                        myPreferences["show_gender"] = genderNumber
                        myPreferences["messages"] = true
                        myPreferences["vibraciones"] = true
                        
                        NSUserDefaults.standardUserDefaults().setValue(2.5, forKey: "distance")
                        NSUserDefaults.standardUserDefaults().setValue(genderString, forKey: "genderSearch")
                        NSUserDefaults.standardUserDefaults().setValue(18, forKey: "age_min")
                        NSUserDefaults.standardUserDefaults().setValue(55, forKey: "age_max")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        myPreferences.saveInBackground()
                        
                        PFUser.currentUser()!.saveInBackgroundWithBlock({succeeded, error in
                            if succeeded{
                                self.navigationController?.setNavigationBarHidden(self.navigationController?.navigationBarHidden == false, animated: false)
                                self.performSegueWithIdentifier("goToLoading", sender: self)
                            }else{
                                JLToast.makeText(NSLocalizedString("Error al guardar información", comment: "")).show()
                            }
                        })
                        
                    })
                    
                }
            }
        }
    }
    func saveLoacation(){
        let point = PFGeoPoint(latitude:self.coordinate.latitude, longitude:self.coordinate.longitude)
        
        let innerP1 = NSPredicate(format: "userId = %@", PFUser.currentUser()?.valueForKey("objectId") as! String)
        var query = PFQuery(className: "preferences", predicate: innerP1)
        
        query.addAscendingOrder("createdAt")
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if var objs = objects as? [PFObject] {
                if objs.count > 0 {
                    
                }else{
                    var myPreferences:PFObject = PFObject(className: "preferences")
                    var groupACL = PFACL()
                    // 1 Hombres
                    // 2 Mujeres
                    var genderNumber = 1
                    var genderString = "male"
                    
                    if PFUser.currentUser()?.valueForKey("gender") as! String == "male" {
                        genderNumber = 2
                        genderString = "female"
                    }
                    
                    myPreferences["userId"] = PFUser.currentUser()?.valueForKey("objectId")
                    myPreferences["found_me"] = true
                    myPreferences["distance"] = 2.5
                    myPreferences["age_min"] = 18
                    myPreferences["age_max"] = 55
                    myPreferences["show_gender"] = genderNumber
                    myPreferences["messages"] = true
                    myPreferences["vibraciones"] = true
                    
                    NSUserDefaults.standardUserDefaults().setValue(2.5, forKey: "distance")
                    NSUserDefaults.standardUserDefaults().setValue(genderString, forKey: "genderSearch")
                    NSUserDefaults.standardUserDefaults().setValue(18, forKey: "age_min")
                    NSUserDefaults.standardUserDefaults().setValue(55, forKey: "age_max")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    myPreferences.saveInBackground()
                }
            }
        }

        
        PFUser.currentUser()!.setValue(point, forKey: "location")
        PFUser.currentUser()!.saveInBackgroundWithBlock({succeeded, error in
            if succeeded{
                
                if (PFUser.currentUser()?.valueForKey("gender") == nil) || (PFUser.currentUser()?.valueForKey("birthday") == nil) {
                    NSUserDefaults.standardUserDefaults().setValue(true, forKey: "isFromRequeried")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    self.performSegueWithIdentifier("goToConfigInfo", sender: self)
                } else {
                    if (NSUserDefaults.standardUserDefaults().valueForKey("goToPush") != nil) {
                        if NSUserDefaults.standardUserDefaults().valueForKey("goToPush") as! Bool {
                            self.performSegueWithIdentifier("goToUserPush", sender: self)
                        } else {
                            self.navigationController?.setNavigationBarHidden(self.navigationController?.navigationBarHidden == false, animated: false)
                            self.performSegueWithIdentifier("goToLoading", sender: self)
                        }
                    } else {
                        self.navigationController?.setNavigationBarHidden(self.navigationController?.navigationBarHidden == false, animated: false)
                        self.performSegueWithIdentifier("goToLoading", sender: self)
                    }
                }
            }else{
                JLToast.makeText(NSLocalizedString("Error al guardar ubicación", comment: "")).show()
            }
        })
    }
    
    func loadCommomFirendsFacebook(opcion : String){
        // Get List Of Friends
        var completionHandler = { connection, result, error in
            //  println(result)
            println(error)
            if result != nil {
                if result.objectForKey("data")!.count > 0 {
                    println(result["data"])
                    PFUser.currentUser()!.setValue(result["data"], forKey: "friends_facebook")
                }
            }
            if opcion == "save" {
                self.saveLoacation()
            } else if opcion == "load" {
                self.loadData()
            }
            
            } as FBRequestHandler;
        var parameterts = ["fields" : "name,installed,first_name,picture"]
        FBRequestConnection.startWithGraphPath("me/friends", parameters: parameterts, HTTPMethod: "GET", completionHandler: completionHandler)
    }
    
    func loadLocationManager(){
        seenError = false
        locationFixArchive = false
        manager = OneShotLocationManager()
        manager!.fetchWithCompletion()
            .onSuccess({location in
                println(location.coordinate.latitude)
                println(location.coordinate.longitude)
                self.coordinate = location.coordinate
                self.makeFacebookLogin()
                
            })
            .onFail({error in
                println(error.localizedDescription)
                 JLToast.makeText(NSLocalizedString("Error al obtener ubicación", comment: "")).show()
                self.clickedLoagin = false
            })
        

    }
    func loadLocationManagerInit(){
        seenError = false
        locationFixArchive = false
        manager = OneShotLocationManager()
        manager!.fetchWithCompletion()
            .onSuccess({location in
                println(location.coordinate.latitude)
                println(location.coordinate.longitude)
                self.coordinate = location.coordinate
                //self.saveLoacation()
                self.loadCommomFirendsFacebook("save");
                
            })
            .onFail({error in
                println(error.localizedDescription)
                JLToast.makeText(NSLocalizedString("Error al obtener ubicación", comment: "")).show()
                self.clickedLoagin = false
            })
        
        
    }
    
    
    func makeFacebookLogin(){
        
        PFFacebookUtils.logInWithPermissions(self.permissions, block: {
            (user: PFUser?, error: NSError?) -> Void in
            
            if user == nil {
                NSLog("Uh oh. The user cancelled the Facebook login.")
            } else if user!.isNew {
                NSLog("User signed up and logged in through Facebook!")
                let installation = PFInstallation.currentInstallation()
                installation["user"] = PFUser.currentUser()
                installation.saveInBackground()
                self.loadCommomFirendsFacebook("load")
                //self.loadData()
                
            } else {
                 NSLog("User logged in through Facebook! \(user!.email)")
                NSLog("\(PFUser.currentUser())")
                let installation = PFInstallation.currentInstallation()
                installation["user"] = PFUser.currentUser()
                installation.saveInBackground()
                //self.saveLoacation()
                self.loadCommomFirendsFacebook("save")
               
            }
            self.clickedLoagin = false
        })

    }

}

