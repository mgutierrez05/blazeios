//
//  ActivaPlusController.swift
//  blaze
//
//  Created by Armando Trujillo Zazueta on 12/10/15.
//  Copyright (c) 2015 Armando Trujillo Zazueta. All rights reserved.
//

import UIKit
import Parse
import StoreKit

class ActivaPlusViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver{
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var spinnerButton: UIActivityIndicatorView!
    @IBOutlet var btnActivaPlus: UIButton!
    @IBOutlet var imgInfor: UIImageView!
    
    var productIDs: Array<String!> = []
    var productsArray: Array<SKProduct!> = []
    var transactionInProgress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productIDs.append("com.nextdots.blaze.blazeplus")
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        
        if PFUser.currentUser()?.valueForKey("isPlus") as! Bool {
            btnActivaPlus.enabled = false
        } else {
            btnActivaPlus.enabled = false
            spinnerButton.startAnimating()
            requestProductInfo()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: Funcs
    func requestProductInfo(){
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<NSObject>)
            productRequest.delegate = self
            productRequest.start()
        } else {
            println("Cannot perform In App Purchases.")
        }
    }
    
    //MARK: Products - Delegate 
    func productsRequest(request: SKProductsRequest!, didReceiveResponse response: SKProductsResponse!) {
        if response.products.count > 0 {
            for product in response.products {
                productsArray.append(product as! SKProduct)
                println((product as! SKProduct).localizedDescription)
                println((product as! SKProduct).localizedTitle)
                btnActivaPlus.enabled = true
                spinnerButton.stopAnimating()
            }
        }
        else {
            println("There are no products.")
        }
        
        if response.invalidProductIdentifiers.count != 0 {
            println(response.invalidProductIdentifiers.description)
        }
    }
    
    func paymentQueue(queue: SKPaymentQueue!, updatedTransactions transactions: [AnyObject]!) {
        for transaction in transactions as! [SKPaymentTransaction] {
            switch transaction.transactionState {
            case SKPaymentTransactionState.Purchased:
                println("Transaction completed successfully.")
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                transactionInProgress = false
                spinner.stopAnimating()
                PFUser.currentUser()?.setValue(true, forKey: "isPlus")
                var date = NSDate()
                PFUser.currentUser()?.setValue(date, forKey: "plusDate")
                PFUser.currentUser()?.saveInBackground()
                btnActivaPlus.enabled = true
                
                UIAlertView(title: NSLocalizedString("Felicidades", comment: ""), message: NSLocalizedString("Has activado Blaze Plus, ya puedes disfrutar de tus beneficios!", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Aceptar", comment: "")).show()

            case SKPaymentTransactionState.Failed:
                println("Transaction Failed");
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                transactionInProgress = false
                spinner.stopAnimating()
                
            default:
                println(transaction.transactionState.rawValue)
            }
        }
    }
    
    //MARK: IBActions - buttons 
    @IBAction func backAction(button: UIButton) {
        if !transactionInProgress {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func activaPlus(button: UIButton) {
        if !transactionInProgress {
            spinner.startAnimating()
            let payment = SKPayment(product: self.productsArray[0] as SKProduct)
            SKPaymentQueue.defaultQueue().addPayment(payment)
            transactionInProgress = true
        }
    }
}

