//
//  ChatsViewController.swift
//  blaze
//
//  Created by devmg on 20/8/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import UIKit
import Parse

class ChatsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var table: UITableView!
    var usrArray: [PFUser]! = [PFUser]()
    
    var tmpUser:PFUser!
    
    var friendshipArray: [PFObject]! = [PFObject]()
    var isNewArr:[Bool] = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getMessageFunc", name: "getMessage", object: nil)
        loadToolBar()
    
        self.navigationController!.interactivePopGestureRecognizer.enabled = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       loadRows()
    }
    
    func loadToolBar(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        let logo = UIImage(named: "chatslogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 43))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = logo
        self.navigationItem.titleView = imageView
        
        self.navigationController!.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.tintColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        self.navigationItem.hidesBackButton = true
        let back = UIImage(named: "backbtnbl")
        
        var backBarButton = UIBarButtonItem(image: back, style: .Plain , target: self, action: "goToBack")
        self.navigationItem.leftBarButtonItem = backBarButton
        self.navigationController?.navigationBar.frame.origin.y = 35
        self.navigationController!.navigationBar.clipsToBounds = true;
        
    }
    func getMessageFunc(){
        loadRows()
    }
    
  
    func goToBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    func loadRows(){
        usrArray.removeAll(keepCapacity: false)
        friendshipArray.removeAll(keepCapacity: false)
        isNewArr.removeAll(keepCapacity: false)
        let innerP1 = NSPredicate(format: "fromUser = %@ ",PFUser.currentUser()!)
        var innerQ1 = PFQuery(className: "friendship", predicate: innerP1)
        
        let innerP2 = NSPredicate(format: "toUser = %@",PFUser.currentUser()!)
        var innerQ2 = PFQuery(className: "friendship", predicate: innerP2)
        
        var query = PFQuery.orQueryWithSubqueries([innerQ1,innerQ2])
//        query.addAscendingOrder("updatedAt")
        //self.friendship["dateLastMessage"] = NSDate()
        query.addDescendingOrder("dateLastMessage")
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                if let obs = objects as? [PFObject]  {
                    for object in obs {
                        var fromUser = object.objectForKey("fromUser") as! PFUser
                        var toUser = object.objectForKey("toUser") as! PFUser
                        if fromUser.objectId == PFUser.currentUser()?.objectId{
                            if object.valueForKey("status") as? String != "pending" {
                                self.usrArray.append(object.objectForKey("toUser") as! PFUser)
                                self.friendshipArray.append(object)
                                if let newMessage = object.objectForKey("newTo") as? Bool {
                                    self.isNewArr.append(newMessage)
                                }else{
                                    self.isNewArr.append(false)
                                }
                            }
                        }else{
                            self.usrArray.append(object.objectForKey("fromUser") as! PFUser)
                            self.friendshipArray.append(object)
                            if let newMessage = object.objectForKey("newFrom") as? Bool {
                                self.isNewArr.append(newMessage)
                            }else{
                                self.isNewArr.append(false)
                            }
                        }
                        
                    }
                    
                }
                
                self.table.reloadData()
            } else {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usrArray != nil ? usrArray.count : 0
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:ChatCell = tableView.dequeueReusableCellWithIdentifier("chatCell", forIndexPath: indexPath) as! ChatCell
        cell.user = usrArray[indexPath.item]
        cell.friendship = friendshipArray[indexPath.item]
        cell.isNewMessage = isNewArr[indexPath.item]
        cell.indexPosition = indexPath
        cell.parentTable = table
        cell.chatController = self
        cell.loadData()
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.whiteColor() 
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func goToMessage(oneUser:PFUser) {
        self.tmpUser = oneUser
        self.performSegueWithIdentifier("goToMessage2", sender: self)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goToMessage2") {
            var svc = segue.destinationViewController as! MessagesController;
            svc.addresseeUser = self.tmpUser
            svc.senderUser = PFUser.currentUser()
            
        }
    }
}
