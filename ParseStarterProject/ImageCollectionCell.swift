//
//  ImageCollectionCell.swift
//  Blaze
//
//  Created by Armando Trujillo Zazueta  on 12/10/15.
//  Copyright © 2015 Armando Trujillo zazueta. All rights reserved.
//

import UIKit
import Parse

class ImageCollectionCell: UICollectionViewCell {


    @IBOutlet weak var imageGallery: UIImageView!
    
    override func awakeFromNib() {
        
        imageGallery.clipsToBounds = true
        imageGallery.contentMode = .ScaleAspectFill
        super.awakeFromNib()
    }
}
