//
//  PreferenciasViewController.swift
//  blaze
//
//  Created by Armando Trujillo Zazueta on 6/10/15.
//  Copyright (c) 2015 Armando Trujillo Zazueta. All rights reserved.
//

import UIKit
import Parse
import Foundation


class PreferenciasViewController: UIViewController, UIActionSheetDelegate {
    
    var descucrimiento = true
    var preferencias:PFObject!
    
    @IBOutlet weak var lblDistancia: UILabel!
    @IBOutlet weak var lblEdad: UILabel!
    @IBOutlet weak var downloandBtn: UIButton!
    
    
    @IBOutlet weak var sliderDistancia: UISlider!
    @IBOutlet weak var sliderEdad: UISlider!
    @IBOutlet weak var viewEdad: UIView!
    @IBOutlet weak var btnDescubrimientoActivado: UIButton!
    @IBOutlet weak var btnDescubrimientoDesactivado: UIButton!
    @IBOutlet weak var btnMostrar: UIButton!
    
    let rangeSlider = RangeSlider(frame: CGRectZero)
    
    override func viewDidLoad() {
        
        if PFUser.currentUser()?.valueForKey("isPlus") as! Bool {
            sliderDistancia.maximumValue = 150.0
        } else {
            sliderDistancia.maximumValue = 10.0
        }
        
        loadPreferencias()
        loadToolBar()
        
        rangeSlider.minimumValue = 10.0
        rangeSlider.maximumValue = 100
        rangeSlider.lowerValue = 18.0
        rangeSlider.upperValue = 55.0

        
        rangeSlider.addTarget(self, action: "rangeSliderTouchUpInside:", forControlEvents: UIControlEvents.TouchUpInside)
        rangeSlider.addTarget(self, action: "rangeSliderValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
       
    }
    
    override func viewDidAppear(animated: Bool) {
        if PFUser.currentUser()?.valueForKey("isPlus") as! Bool {
            sliderDistancia.maximumValue = 150.0
        } else {
            sliderDistancia.maximumValue = 10.0
        }
    }
    
    override func viewDidLayoutSubviews() {
        print(UIScreen.mainScreen().bounds.size.width);
        if UIScreen.mainScreen().bounds.size.height == 480 {
            //downloandBtn.frame.size.height = 100;
            let heightConstraint = NSLayoutConstraint(item: downloandBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100)
            view.addConstraint(heightConstraint)
        } else {
            let heightConstraint = NSLayoutConstraint(item: downloandBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 150)
            view.addConstraint(heightConstraint)
        }
        
        var frame = sliderEdad.frame
        frame.size.height = 18
        frame.origin.y += 7
        frame.origin.x = 0
        rangeSlider.frame = frame
        rangeSlider.trackHighlightTintColor = UIColor.clearColor()
        rangeSlider.trackTintColor = UIColor.clearColor()
        rangeSlider.thumbTintColor = UIColor.clearColor()
        sliderEdad.thumbTintColor = UIColor.clearColor()
        //rangeSlider.lowerThumbLayer.contents = UIImage(named: "red-circle")?.CGImage
        //rangeSlider.lowerThumbLayer.contentsGravity = kCAGravityCenter
        //sliderEdad.hidden = true
         viewEdad.addSubview(rangeSlider)
    }
    
    func loadToolBar(){
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationItem.hidesBackButton = true

        self.navigationController?.navigationBar.frame.origin.y = 35
        self.navigationController!.navigationBar.clipsToBounds = true;
        
        let image = UIImage(named: "red-circle")
        
        sliderDistancia.setThumbImage(image, forState: UIControlState.Normal)
//        sliderEdad.setThumbImage(image, forState: UIControlState.Normal)
    }
    
    @IBAction func goToBack(botton: UIButton){
        //labelTitle.removeFromSuperview()
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func actionChangeDescubrimiento(botton: UIButton){
        if botton.tag == 1 {
            preferencias["found_me"] = true
            btnDescubrimientoActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
            btnDescubrimientoDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        } else if botton.tag == 2 {
            preferencias["found_me"] = false
            btnDescubrimientoActivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
            btnDescubrimientoDesactivado.setBackgroundImage(UIImage(named: "red-circle"), forState: UIControlState.Normal)
        }
        preferencias.saveInBackground()
    }
    
    func loadPreferencias(){
        let innerP1 = NSPredicate(format: "userId = %@", PFUser.currentUser()?.valueForKey("objectId") as! String)
        var query = PFQuery(className: "preferences", predicate: innerP1)

        query.addAscendingOrder("createdAt")
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if var objs = objects as? [PFObject] {
                if objs.count > 0 {
                    self.preferencias = objs[0]
                    
                    if self.preferencias.valueForKey("found_me") as! Bool {
                        self.btnDescubrimientoActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
                        self.btnDescubrimientoDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                    } else {
                        self.btnDescubrimientoActivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                        self.btnDescubrimientoDesactivado.setBackgroundImage(UIImage(named: "red-circle"), forState: UIControlState.Normal)
                    }
                    
                    self.lblDistancia.text = String(format: "%.2f KM", self.preferencias.valueForKey("distance") as! Float)
                    self.sliderDistancia.value = self.preferencias.valueForKey("distance") as! Float
                    let age_min = self.preferencias.valueForKey("age_min") as! Int
                    let age_max = self.preferencias.valueForKey("age_max") as! Int
                    self.lblEdad.text = "\(age_min)-\(age_max)"
                    self.rangeSlider.lowerValue = Double(age_min)
                    self.rangeSlider.upperValue = Double(age_max)
                    
                    let show_gender = self.preferencias.valueForKey("show_gender") as! Int
                    
                    if show_gender == 1 {
                        self.btnMostrar.setTitle(NSLocalizedString("Solo Hombres", comment: ""), forState: .Normal)
                    } else if show_gender == 2 {
                        self.btnMostrar.setTitle(NSLocalizedString("Solo Mujeres", comment: ""), forState: .Normal)
                    } else if show_gender == 3 {
                        self.btnMostrar.setTitle(NSLocalizedString("Hombres/Mujeres", comment: ""), forState: .Normal)
                    }
                
                }else{
                    var myPreferences:PFObject = PFObject(className: "preferences")
                    var groupACL = PFACL()
                    // 1 Hombres
                    // 2 Mujeres
                    var genderNumber = 1
                    var genderString = "male"
                    
                    if PFUser.currentUser()?.valueForKey("gender") as! String == "male" {
                        genderNumber = 2
                        genderString = "female"
                    }
                    
                    myPreferences["userId"] = PFUser.currentUser()?.valueForKey("objectId")
                    myPreferences["found_me"] = true
                    myPreferences["distance"] = 2.5
                    myPreferences["age_min"] = 18
                    myPreferences["age_max"] = 55
                    myPreferences["show_gender"] = genderNumber
                    myPreferences["messages"] = true
                    myPreferences["vibraciones"] = true
                    
                    myPreferences.saveInBackground()
                    self.preferencias = myPreferences
                    
                    NSUserDefaults.standardUserDefaults().setValue(2.5, forKey: "distance")
                    NSUserDefaults.standardUserDefaults().setValue(genderString, forKey: "genderSearch")
                    NSUserDefaults.standardUserDefaults().setValue(18, forKey: "age_min")
                    NSUserDefaults.standardUserDefaults().setValue(55, forKey: "age_max")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    
                    
                    self.btnDescubrimientoActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
                    self.btnDescubrimientoDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                    
                    self.lblDistancia.text = "2.5 KM"
                    self.sliderDistancia.value = 2.5
                    self.lblEdad.text = "18-55"
                    
                    if genderNumber == 1 {
                        self.btnMostrar.setTitle(NSLocalizedString("Solo Hombres", comment: ""), forState: .Normal)
                    } else if genderNumber == 2 {
                        self.btnMostrar.setTitle(NSLocalizedString("Solo Mujeres", comment: ""), forState: .Normal)
                    }
                    
                }
            }
        }
    }

    
    func goToChat(){
        
    }
    
    @IBAction func showOptions(button: UIButton){
        let actionSheet = UIActionSheet(title: NSLocalizedString("¿Que deseas Buscas?", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancelar", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: NSLocalizedString("Solo Hombres", comment: ""), NSLocalizedString("Solo Mujeres", comment: ""), NSLocalizedString("Hombres/Mujeres", comment: ""))
        
        actionSheet.showInView(self.view)
    }

    @IBAction func showBlazePlus(button: UIButton){
        var ActivaPlus = self.storyboard?.instantiateViewControllerWithIdentifier("ActivaPlusViewController") as! ActivaPlusViewController
        self.presentViewController(ActivaPlus, animated: true, completion: nil)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        println("\(buttonIndex)")
        switch (buttonIndex){
            
        case 0:
            println("Cancelar")
        case 1:
            println("Solo Hombres")
            btnMostrar.setTitle(NSLocalizedString("Solo Hombres", comment: ""), forState: .Normal)
            preferencias["show_gender"] = 1
            preferencias.saveInBackground()
            NSUserDefaults.standardUserDefaults().setValue("male", forKey: "genderSearch")
            NSUserDefaults.standardUserDefaults().synchronize()
        case 2:
            println("Solo Mujeres")
            btnMostrar.setTitle(NSLocalizedString("Solo Mujeres", comment: ""), forState: .Normal)
            preferencias["show_gender"] = 2
            preferencias.saveInBackground()
            NSUserDefaults.standardUserDefaults().setValue("female", forKey: "genderSearch")
            NSUserDefaults.standardUserDefaults().synchronize()
        case 3:
            println("Hombres/Mujeres")
            btnMostrar.setTitle(NSLocalizedString("Hombres/Mujeres", comment: ""), forState: .Normal)
            preferencias["show_gender"] = 3
            preferencias.saveInBackground()
            NSUserDefaults.standardUserDefaults().removeObjectForKey("genderSearch")
            NSUserDefaults.standardUserDefaults().synchronize()
        default:
            println("Default")
        }
    }
    
    @IBAction func slideDistancia(sender: UISlider) {
        lblDistancia.text = String(format: "%.2f KM", sender.value)
        preferencias["distance"] = sender.value
        preferencias.saveInBackground()
        NSUserDefaults.standardUserDefaults().setValue(sender.value, forKey: "distance")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    @IBAction func slideEdad(sender: UISlider) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
//        if (segue.identifier == "goToUserList") {
//            var svc = segue.destinationViewController as! UserListViewController;            
//            svc.arrayInformationMessages = self.arrayInformationMessages
//            
//        }
//        if (segue.identifier == "dimmisLoading") {
//            var svc = segue.destinationViewController as! UserListViewController;
//            svc.arrayInformationMessages = self.arrayInformationMessages
//            
//        }
    }
    
    //MARK: RangeSlider - Delegates
    func rangeSliderValueChanged(rangeSlider: RangeSlider) {
        lblEdad.text = "\(Int(round(rangeSlider.lowerValue)))-\(Int(round(rangeSlider.upperValue)))"
    }
    
    func rangeSliderTouchUpInside(rangeSlider: RangeSlider) {
        preferencias.setValue(Int(round(rangeSlider.lowerValue)), forKey: "age_min")
        preferencias.setValue(Int(round(rangeSlider.upperValue)), forKey: "age_max")
        NSUserDefaults.standardUserDefaults().setValue(rangeSlider.lowerValue, forKey: "age_min")
        NSUserDefaults.standardUserDefaults().setValue(rangeSlider.upperValue, forKey: "age_max")
        preferencias.saveInBackground()
    }
}