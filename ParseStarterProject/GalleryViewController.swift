//
//  GalleryViewController.swift
//  Blaze
//
//  Created by Armando Trujillo Zazueta  on 12/10/15.
//  Copyright © 2015 Armando Trujillo zazueta. All rights reserved.
//

import UIKit
import Parse

class GalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var array: NSMutableArray!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var collectionView: UICollectionView!
    
    
    @IBOutlet weak var btnAtras: UIBarButtonItem!
    @IBOutlet weak var btnSeleccionar: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAtras.title = NSLocalizedString("Atras", comment: "")
        btnSeleccionar.title = ""
        btnSeleccionar.enabled = false
        imageView.hidden = true
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCollectionCell", forIndexPath: indexPath) as! ImageCollectionCell
        
        let dataItem = array.objectAtIndex(indexPath.row) as! NSDictionary
        let images = dataItem.objectForKey("images") as! NSArray
        let item = images.objectAtIndex(0) as! NSDictionary
        
        var urlString = item.objectForKey("source") as! String
        var imgURL: NSURL = NSURL(string: urlString)!
        let url = NSURL(string: urlString)
        //let dataImage = NSData(contentsOfURL: url!)
        //cell.imageGallery.image = UIImage(data: dataImage!)
        downloadImage(url!, imageView: cell.imageGallery)
        
        //cell.imageGallery.image = UIImage(named: "loco2")
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let dataItem = array.objectAtIndex(indexPath.row) as! NSDictionary
        let images = dataItem.objectForKey("images") as! NSArray
        let item = images.objectAtIndex(0) as! NSDictionary
        
        var urlString = item.objectForKey("source") as! String
        var imgURL: NSURL = NSURL(string: urlString)!
        let url = NSURL(string: urlString)
        downloadImage(url!, imageView: imageView)
        
        collectionView.hidden = true
        imageView.hidden = false
        btnAtras.title = NSLocalizedString("Cancelar", comment: "")
        btnSeleccionar.title = NSLocalizedString("Seleccionar", comment: "")
        btnSeleccionar.enabled = true
    }
    
    
    @IBAction func actionBackCancel(button: AnyObject){
        if imageView.hidden {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            btnAtras.title = NSLocalizedString("Atras", comment: "")
            btnSeleccionar.title = ""
            btnSeleccionar.enabled = false
            imageView.hidden = true
            collectionView.hidden = false
            imageView.image = UIImage()
        }
    }
    
    @IBAction func selectImage(button: AnyObject) {
        var imageData = UIImagePNGRepresentation(imageView.image)
        NSUserDefaults.standardUserDefaults().setValue(imageData, forKey: "imageFacebook")
        NSUserDefaults.standardUserDefaults().synchronize()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }
    func downloadImage(url: NSURL, imageView: UIImageView){
        print("Started downloading \"\(url.URLByDeletingPathExtension!.lastPathComponent!)\".")
        getDataFromUrl(url) { (data, response, error)  in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                //guard; let data = data; where error == nil else { return }
                print("Finished downloading \"\(url.URLByDeletingPathExtension!.lastPathComponent!)\".")
                imageView.image = UIImage(data: data!)
            }
        }
    }
    
}
