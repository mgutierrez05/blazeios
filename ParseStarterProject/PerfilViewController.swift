//
//  PerfilViewController.swift
//  blaze
//
//  Created by Armando Trujillo Zazueta on 6/10/15.
//  Copyright (c) 2015 Armando Trujillo Zazueta. All rights reserved.
//

import UIKit
import Parse
import Foundation

class PerfilViewController: UIViewController , UITextViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate , UIImagePickerControllerDelegate, UIAlertViewDelegate {
   
    var keyBoardStatus = false
    var chatBarButton:UIBarButtonItem!
    var descripcionText = ""
    var datePicker:UIDatePicker!
    @IBOutlet weak var viewSaveImg: UIView!
    @IBOutlet var txtDate:UITextField!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAcerca: UILabel!
    @IBOutlet weak var lblCountDescrip: UILabel!
    @IBOutlet weak var txtViewDescripcion: UITextView!
    @IBOutlet weak var btnSexoM: UIButton!
    @IBOutlet weak var btnSexoF: UIButton!
    @IBOutlet weak var imgActivado: UIImageView!
    
    @IBOutlet weak var imgUserCenter: UIImageView!
    @IBOutlet weak var imgUserLeft : UIImageView!
    @IBOutlet weak var imgUserRight : UIImageView!
    @IBOutlet weak var btnUserCenter: UIButton!
    @IBOutlet weak var btnUserLeft: UIButton!
    @IBOutlet weak var btnUserRight: UIButton!
    @IBOutlet weak var btnUserDelCenter: UIButton!
    @IBOutlet weak var btnUserDelLeft: UIButton!
    @IBOutlet weak var btnUserDelRight: UIButton!
    
    @IBOutlet weak var lblVerPerfil: UILabel!
    @IBOutlet weak var lblfecha: UILabel!
    @IBOutlet weak var lblLabel1: UILabel!
    @IBOutlet weak var lblLabel2: UILabel!
    @IBOutlet weak var btnPreferencia: UIButton!
    @IBOutlet weak var btnAjuste: UIButton!
    @IBOutlet weak var imgFrameLeft: UIImageView!
    @IBOutlet weak var imgFrameRigth: UIImageView!
    @IBOutlet weak var imgFrameCenter: UIImageView!
    @IBOutlet weak var viewImagenes: UIView!
    @IBOutlet weak var lblH: UILabel!
    @IBOutlet weak var lblM: UILabel!
    @IBOutlet weak var viewSexo: UIView!
    
    //@IBOutlet weak var scrollView: UIScrollView!
    var onlyOne = true
    @IBOutlet weak var scrollView : UIScrollView!
    
    override func viewDidLoad() {
        initDatesPickers()
        loadToolBar()
        registerNotification()
        addDoneButtonOnKeyboard()
        setImageScale();
        self.view.bringSubviewToFront(viewSaveImg)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
    }
    
    override func viewWillLayoutSubviews() {
       
    }
    
    override func viewDidLayoutSubviews() {
        let heightConstraintPrivacidad = NSLayoutConstraint(item: txtViewDescripcion, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 46)
        view.addConstraint(heightConstraintPrivacidad)
    }
    
    override func viewDidAppear(animated: Bool) {
        if (NSUserDefaults.standardUserDefaults().valueForKey("imageFacebook") != nil) {
            let base64String = NSUserDefaults.standardUserDefaults().valueForKey("imageFacebook") as! NSData
            var decodedimage = UIImage(data: base64String)
            NSUserDefaults.standardUserDefaults().removeObjectForKey("imageFacebook")
            NSUserDefaults.standardUserDefaults().synchronize()
            setImage(decodedimage!)
        } else {
            loadUserInfo()
        }
    }
    func setImageScale(){
        
        setImageScale(imgUserCenter);
        setImageScale(imgUserLeft);
        setImageScale(imgUserRight);
        
    }
    func setImageScale(iv : UIImageView){
        iv.clipsToBounds = true
        iv.contentMode = .ScaleAspectFill
    }
    //MARK: Helpers - Loading
    func registerNotification(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    func initDatesPickers () {
        //Date Inicio
        var customView:UIView = UIView (frame: CGRectMake(0, 100, 320, 160))
        customView.backgroundColor = UIColor.whiteColor()
        datePicker = UIDatePicker(frame: CGRectMake(0, 0, 320, 160))
        datePicker.datePickerMode = UIDatePickerMode.Date
        customView .addSubview(datePicker)
        txtDate.inputView = customView
        
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.Default
        
        var flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        var done: UIBarButtonItem = UIBarButtonItem(title:  NSLocalizedString("Aceptar", comment: ""), style: UIBarButtonItemStyle.Done, target: self, action: Selector("datePickerSelectedInicio"))
        var Cancel: UIBarButtonItem = UIBarButtonItem(title:  NSLocalizedString("Cancelar", comment: ""), style: UIBarButtonItemStyle.Done, target: self, action: Selector("CancelButtonAction"))
        
        var items = NSMutableArray()
        items.addObject(Cancel)
        items.addObject(flexSpace)
        items.addObject(done)
        
        
        doneToolbar.items = items as [AnyObject]
        doneToolbar.sizeToFit()
        
        txtDate.inputAccessoryView = doneToolbar
    }
    
    func loadUserInfo() {
        var user:PFObject = PFUser.currentUser()!
        //lblUserName.text = PFUser.currentUser()?.username
        let userName:String = (user.valueForKey("username") as? String)!
        var gender = ""
        
        if (user.valueForKey("gender") != nil){
             gender = (user.valueForKey("gender") as? String)!
        }
        
        var birthday = NSDate()
        if (user.valueForKey("birthday") != nil) {
            birthday = (user.valueForKey("birthday") as? NSDate)!
        }
        
        datePicker.date = birthday
        
        if ((user.valueForKey("description_info") as? String) != nil) {
            descripcionText = user.valueForKey("description_info") as! String
            lblCountDescrip.text = "\(500-(descripcionText as NSString).length) " + NSLocalizedString("caracteres", comment: "")
        }
        
        if user.valueForKey("isPlus") as! Bool {
            imgActivado.image = UIImage(named: NSLocalizedString("activado-on", comment: ""))
        } else {
            imgActivado.image = UIImage(named: NSLocalizedString("activado-off", comment: ""))
        }
        
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        formatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        lblUserName.text = userName
        lblAcerca.text = NSLocalizedString("Acerca de", comment: "") + " \(userName):"
        txtDate.text = formatter.stringFromDate(birthday)
        txtViewDescripcion.text = descripcionText
        
        if gender == "male" {
            btnSexoM.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
            btnSexoF.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        } else if gender == "female" {
            btnSexoF.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
            btnSexoM.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        } else {
            btnSexoM.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
            btnSexoF.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        }
        
        if var image:PFFile = user.valueForKey("profilePicture") as? PFFile {
            image.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        let image = UIImage(data:imageData)
                        self.imgUserCenter.image = image!
                        self.btnUserCenter.hidden = true
                        self.btnUserDelCenter.hidden = false
                    } else {
                        self.btnUserCenter.hidden = false
                        self.btnUserDelCenter.hidden = true
                    }
                }
            }
        } else {
            self.btnUserCenter.hidden = false
            self.btnUserDelCenter.hidden = true
        }
        
        if  var imageLeft:PFFile = user.valueForKey("pictureLeft") as? PFFile {
            imageLeft.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        let image = UIImage(data:imageData)
                        self.imgUserLeft.image = image!
                        self.btnUserLeft.hidden = true
                        self.btnUserDelLeft.hidden = false
                    } else {
                        self.btnUserLeft.hidden = false
                        self.btnUserDelLeft.hidden = true
                    }
                }
            }
        } else {
            self.btnUserLeft.hidden = false
            self.btnUserDelLeft.hidden = true
        }
        
        if var imageRight:PFFile = user.valueForKey("pictureRight") as? PFFile {
            imageRight.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        let image = UIImage(data:imageData)
                        self.imgUserRight.image = image!
                        self.btnUserRight.hidden = true
                        self.btnUserDelRight.hidden = false
                    } else {
                        self.btnUserRight.hidden = false
                        self.btnUserDelRight.hidden = true
                    }
                }
            }
        } else {
            self.btnUserRight.hidden = false
            self.btnUserDelRight.hidden = true
        }
    }
    
    func loadToolBar(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        let logo = UIImage(named: "config-red.png")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 43))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = logo
        self.navigationItem.titleView = imageView
        
        self.navigationController!.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.tintColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        self.navigationItem.hidesBackButton = true
        let back = UIImage(named: "backarrow.png")
        let chat = UIImage(named: "backbtnbl.png")
        
        var configBarButton = UIBarButtonItem(image: back, style: .Plain , target: self, action: "goToBack")
        chatBarButton = UIBarButtonItem(image: chat, style: .Done, target: self, action: "goToBack")
        //        chatBarButton.tintColor = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        //self.navigationItem.leftBarButtonItem = configBarButton
        self.navigationItem.rightBarButtonItem = chatBarButton
        self.navigationController?.navigationBar.frame.origin.y = 35
        self.navigationController!.navigationBar.clipsToBounds = true;
        
        txtViewDescripcion.layer.borderWidth = 1
        txtViewDescripcion.layer.borderColor = UIColor.grayColor().CGColor
        
    }
    
    func addDoneButtonOnKeyboard()
    {
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.Default
        
        var flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        var done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Aceptar", comment: ""), style: UIBarButtonItemStyle.Done, target: self, action: Selector("doneButtonAction"))
        var Cancel: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancelar", comment: ""), style: UIBarButtonItemStyle.Done, target: self, action: Selector("CancelButtonAction"))
        
        var items = NSMutableArray()
        items.addObject(Cancel)
        items.addObject(flexSpace)
        items.addObject(done)
        
        
        doneToolbar.items = items as [AnyObject]
        doneToolbar.sizeToFit()
        
        txtViewDescripcion.inputAccessoryView = doneToolbar
    }
    
    //MARK: DatePicket - Selected
    
    func datePickerSelectedInicio() {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        formatter.timeStyle = NSDateFormatterStyle.NoStyle
        txtDate.text = formatter.stringFromDate(datePicker.date)
        txtDate.resignFirstResponder()
        
        PFUser.currentUser()?.setValue(datePicker.date, forKey: "birthday")
        PFUser.currentUser()?.saveInBackground()
    }

    //MARK: IBActions - buttons
    @IBAction func changeSex(button: UIButton) {
        if button.tag == 1 {
            PFUser.currentUser()?.setValue("male", forKey: "gender")
            btnSexoM.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
            btnSexoF.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        } else if button.tag == 2 {
            PFUser.currentUser()?.setValue("female", forKey: "gender")
            btnSexoM.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
            btnSexoF.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
        }
        PFUser.currentUser()?.saveInBackground()
    }
    
    func doneButtonAction() {
        NSLog("Aceptar Descripcion")
        txtViewDescripcion.resignFirstResponder()
        //llamar servicios para guardar descripcion
        descripcionText = txtViewDescripcion.text
        PFUser.currentUser()!.setObject(txtViewDescripcion.text, forKey: "description_info")
        PFUser.currentUser()!.saveInBackground()
    }
    
    func CancelButtonAction() {
        NSLog("Cancelar Descripcion")
        txtViewDescripcion.resignFirstResponder()
        txtViewDescripcion.text = descripcionText
        
        txtDate.resignFirstResponder()
        datePicker.date = PFUser.currentUser()?.valueForKey("birthday") as! NSDate
    }

    var imagePicker: UIImagePickerController!
    var indexPhotoSelect = 0
    
    @IBAction func selectOptionPhoto(button: UIButton) {
        
        if button.tag == 21 {
            indexPhotoSelect = 1
        } else if button.tag == 22 {
            indexPhotoSelect = 2
        } else if button.tag == 23 {
            indexPhotoSelect = 3
        }
        
        let actionSheet = UIActionSheet(title: NSLocalizedString("Selecciona una opcion:", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancelar", comment: ""), destructiveButtonTitle: nil, otherButtonTitles:  NSLocalizedString("Importar Desde Facebook", comment: ""))
//         let actionSheet = UIActionSheet(title: NSLocalizedString("Selecciona una opcion:", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancelar", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: NSLocalizedString("Tomar Foto", comment: ""), NSLocalizedString("Seleccionar Foto", comment: ""),NSLocalizedString("Importar Desde Facebook", comment: ""))
        actionSheet.tag = 13
        actionSheet.showInView(self.view)
    }
    
    @IBAction func deletePhoto(button: UIButton) {
        if button.tag == 31 {
            indexPhotoSelect = 1
        } else if button.tag == 32 {
            indexPhotoSelect = 2
        } else if button.tag == 33 {
            indexPhotoSelect = 3
        }
        
        let alert = UIAlertView(title: NSLocalizedString("Eliminando Imagen", comment: ""), message: NSLocalizedString("¿Estas Seguro de Eliminar la Foto?", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("No", comment: ""), otherButtonTitles: NSLocalizedString("Si", comment: ""))
        alert.tag = 13
        alert.show()
    }
    
    // MARK: UIAlertView - delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 13 {
            if buttonIndex == 0 {
                indexPhotoSelect = 0
            } else if buttonIndex == 1 {
                if indexPhotoSelect == 1 {
                    imgUserLeft.image = UIImage()
                    btnUserDelLeft.hidden = true
                    btnUserLeft.hidden = false
                    // Eliminar imagen en parse
                    //PFUser.currentUser()!.setObject(NSNull(), forKey: "pictureLeft")
                    PFUser.currentUser()?.removeObjectForKey("pictureLeft")
                    PFUser.currentUser()!.saveInBackground()
                } else if indexPhotoSelect == 2 {
                    imgUserCenter.image = UIImage()
                    btnUserDelCenter.hidden = true
                    btnUserCenter.hidden = false
                    // guardar imagen en parse
//                    PFUser.currentUser()!.setObject(NSNull(), forKey: "profilePicture")
                    PFUser.currentUser()?.removeObjectForKey("profilePicture")
                    PFUser.currentUser()!.saveInBackground()
                } else if indexPhotoSelect == 3 {
                    imgUserRight.image = UIImage()
                    btnUserDelRight.hidden = true
                    btnUserRight.hidden = false
                    // guardar imagen en parse
                    //PFUser.currentUser()!.setObject(NSNull(), forKey: "pictureRight")
                    PFUser.currentUser()?.removeObjectForKey("pictureRight")
                    PFUser.currentUser()!.saveInBackground()
                }
            }
        }
    }
    
    // MARK: ActionSheet - delegate
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if actionSheet.tag == 13 {
            if buttonIndex == 0 {
                indexPhotoSelect = 0
//            } else if buttonIndex == 1 {
//                //tomar foto 
//                imagePicker =  UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = .Camera
//                imagePicker.allowsEditing = true
//                presentViewController(imagePicker, animated: true, completion: nil)
//            } else if buttonIndex == 2 {
//                //Seleccionar foto
//                imagePicker =  UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//                imagePicker.allowsEditing = true
//                presentViewController(imagePicker, animated: true, completion: nil)
            } else if buttonIndex == 1 {
                //self.performSegueWithIdentifier("pushAlbum", sender: self)
                let albumView  = self.storyboard?.instantiateViewControllerWithIdentifier("navigationAlbum") as! UINavigationController
                self.presentViewController(albumView, animated: true, completion: nil)       
            }
        }
    }
    
    //MARK: UIImagePicke - Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        setImage(info[UIImagePickerControllerEditedImage] as! UIImage)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        indexPhotoSelect = 0
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: Helpers 
    func setImage(image: UIImage){
        self.view.bringSubviewToFront(viewSaveImg)
        viewSaveImg.hidden = false
        navigationController?.navigationBar.hidden = true
        if indexPhotoSelect == 1 {
            imgUserLeft.image = image
            btnUserDelLeft.hidden = false
            btnUserLeft.hidden = true
            // guardar imagen en parse
//            NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
            var imageData = NSData(data: UIImageJPEGRepresentation(image, 0.5))
            println(imageData.length)
            var picture = PFFile(data: imageData)
            PFUser.currentUser()!.setObject(picture, forKey: "pictureLeft")
            PFUser.currentUser()!.saveInBackground()
            PFUser.currentUser()?.saveInBackgroundWithBlock({
                succeeded, error in
                if succeeded {
                    self.viewSaveImg.hidden = true
                    self.navigationController?.navigationBar.hidden = false
                }
            })
        } else if indexPhotoSelect == 2 {
            imgUserCenter.image = image
            btnUserDelCenter.hidden = false
            btnUserCenter.hidden = true
            // guardar imagen en parse
            var imageData = NSData(data: UIImageJPEGRepresentation(image, 0.5))
            var picture = PFFile(data: imageData)
            PFUser.currentUser()!.setObject(picture, forKey: "profilePicture")
            PFUser.currentUser()?.saveInBackgroundWithBlock({
                succeeded, error in
                if succeeded {
                    self.viewSaveImg.hidden = true
                    self.navigationController?.navigationBar.hidden = false
                }
            })
        } else if indexPhotoSelect == 3 {
            imgUserRight.image = image
            btnUserDelRight.hidden = false
            btnUserRight.hidden = true
            // guardar imagen en parse
            var imageData = NSData(data: UIImageJPEGRepresentation(image, 0.5))
            var picture = PFFile(data: imageData)
            PFUser.currentUser()!.setObject(picture, forKey: "pictureRight")
            PFUser.currentUser()?.saveInBackgroundWithBlock({
                succeeded, error in
                if succeeded {
                    self.viewSaveImg.hidden = true
                    self.navigationController?.navigationBar.hidden = false
                }
            })
        }
    }
    
    //MARK: Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if ( segue.identifier == "pushAlbum") {
            var sourceViewController = segue.sourceViewController as! UIViewController
            var destinationViewController = segue.destinationViewController as! UIViewController
    
            var transition: CATransition = CATransition()
    
            transition.duration = 0.40
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
            transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    
            sourceViewController.navigationController?.view.layer.addAnimation(transition, forKey: "kCATransition")
            //sourceViewController.navigationController?.pushViewController(destinationViewController, animated: false)
    
        }
    }

    func goToBack(){
        if (PFUser.currentUser()?.valueForKey("gender") == nil) || (PFUser.currentUser()?.valueForKey("birthday") == nil) {
            if (PFUser.currentUser()?.valueForKey("gender") == nil) {
                UIAlertView(title: NSLocalizedString("Campo requerido", comment: ""), message: NSLocalizedString("Seleccione su sexo", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Aceptar", comment: "")).show()
            } else {
                UIAlertView(title: NSLocalizedString("Campo requerido", comment: ""), message: NSLocalizedString("Seleccione su fecha de nacimiento", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Aceptar", comment: "")).show()
            }
        } else {
            if (NSUserDefaults.standardUserDefaults().valueForKey("isFromRequeried") != nil) {
                if NSUserDefaults.standardUserDefaults().valueForKey("isFromRequeried") as! Bool {
                    self.navigationController?.setNavigationBarHidden(self.navigationController?.navigationBarHidden == false, animated: false)
                    NSUserDefaults.standardUserDefaults().removeObjectForKey("isFromRequeried")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }
            NSUserDefaults.standardUserDefaults().setValue(true, forKey: "reloadSearch")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            var transition: CATransition = CATransition()
            
            transition.duration = 0.25
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
            transition.subtype = kCATransitionFromRight; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
            
            self.navigationController?.view.layer.addAnimation(transition, forKey: "kCATransition")
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func goToPreferencias(button: UIButton){
        if !keyBoardStatus {
            self.performSegueWithIdentifier("goToPreferencias", sender: self)
        }
    }
    
    @IBAction func goToAjustes(button: UIButton){
        if !keyBoardStatus {
            self.performSegueWithIdentifier("goToAjustes", sender: self)
        }
    }
    
    //MARK: TextView - Delegate
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        println(text)
        println(range.length)
        var lenght = (textView.text as NSString).length
        lenght += (text as NSString).length
        if lenght > 500 && text != "" {
            return false
        }
        
        if text == "" {
            lenght -= range.length
        }
        
        lblCountDescrip.text = "\(500-lenght) " + NSLocalizedString("caracteres", comment: "")
        
        if (500-lenght) == 0 {
            lblCountDescrip.textColor = UIColor.redColor()
        } else {
            lblCountDescrip.textColor = UIColor.grayColor()
        }
        
        return true
    }

    //MARK: KeyBoard
    func keyboardWillShow(notification: NSNotification) {
        
        if !keyBoardStatus && !txtDate.isFirstResponder() {
            navigationController?.navigationBar.hidden = true
            keyBoardStatus = true
            self.view.frame.origin.y -= 170
            txtDate.enabled = false
    
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if keyBoardStatus && !txtDate.isFirstResponder() {
            navigationController?.navigationBar.hidden = false
            keyBoardStatus = false
            self.view.frame.origin.y += 170
            txtDate.enabled = true
        }
    }
}