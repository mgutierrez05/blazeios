//
//  UIViewExtension.swift
//  blaze
//
//  Created by Armando Trujillo zazueta on 19/02/16.
//  Copyright (c) 2016 DevMg. All rights reserved.
//

import Foundation
extension UIView {
    func removeConstraints() {
        var list = [AnyObject]()
        for c in self.superview!.constraints() {
            if c.firstItem as? UIView == self || c.secondItem as? UIView == self {
                list.append(c)
            }
        }
        self.superview!.removeConstraints(list)
        self.removeConstraints(self.constraints())
    }
}