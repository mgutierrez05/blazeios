
import UIKit
import Parse
import Foundation

class MessagesController: UIViewController,UIScrollViewDelegate, UITextViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var frameMessageView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var frameImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var goToInfo: UIButton!
    @IBOutlet weak var sendMessageBtn: UIButton!

    
    let messagePlaceholeder = UILabel(frame: CGRectMake(5,5,200,20))
    var messageArray = [String]()
    var messageCount = 0
    var scrollViewOriginalY:CGFloat = 126.0
    var scrollViewSizeHeight : CGFloat!
    var frameMessageOriginalY:CGFloat = UIScreen.mainScreen().bounds.height - 50
    
    var messageX:CGFloat = 15.0
    var messageY:CGFloat = 0.0
    let formatter =  NSDateFormatter()
    var senderUser:PFUser!
    var addresseeUser:PFUser!
    
    var friendship:PFObject!
    
    var userNameText:String!
    
    var photo:UIImage!
    
    var isFriendShip = false
    
    override func viewDidLoad() {
//        var myImage = UIImage(named: "aqua")!
//        let myInsets : UIEdgeInsets = UIEdgeInsetsMake(10, 20, 20, 20)
//        var myImage2 = UIImage(named: "orange")!
//        let myInsets2 : UIEdgeInsets = UIEdgeInsetsMake(10,20,20,20)
//        myImage = myImage.resizableImageWithCapInsets(myInsets)
//        myImage2 = myImage2.resizableImageWithCapInsets(myInsets2)
//        bubble.image = myImage
//        bubble2.image = myImage2
        messagePlaceholeder.text = NSLocalizedString("Escriba un mensaje...", comment: "")
        let sendTitle:String = NSLocalizedString("Enviar", comment: "")
        if self.sendMessageBtn != nil {
            self.sendMessageBtn.setTitle(sendTitle, forState: UIControlState.Normal)
        }
        messagePlaceholeder.backgroundColor = UIColor.clearColor()
        messagePlaceholeder.textColor = UIColor.lightGrayColor()
        messagePlaceholeder.hidden = false
        messagePlaceholeder.layer.zPosition = 45
        messageTextView.addSubview(messagePlaceholeder)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getMessageFunc", name: "getMessage", object: nil)
        
        let tapScrollViewGesture = UITapGestureRecognizer(target: self, action: "didTapScrollView")
        tapScrollViewGesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tapScrollViewGesture)
        
        formatter.dateFormat = "HH:mm"
        scrollView.scrollEnabled = true
        
        self.navigationItem.hidesBackButton = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        loadUIConfiguration()
        loadData()
        
        refreshResult()
        
        checkRelation({
            isEnable in
            if isEnable {
                println("Son amigos");
            } else {
                println("No son amigos");
            }
        })
        
        scrollViewSizeHeight = scrollView.frame.size.height
    }
    func checkRelation(callback:(isEnable:Bool)->Void){
        if isFriendShip {
            if self.messageTextView.text != "" {
                self.friendship["lastMessage"] = self.messageTextView.text
                self.friendship["dateLastMessage"] = NSDate()
                self.friendship.saveInBackground()
            }
            callback(isEnable: true)
            return
        }
        
        self.friendshipQuery({
            (mFriendship:PFObject!)->Void in
            if let status = mFriendship.valueForKey("status") as? String{
                if status == "accepted" {
                    self.isFriendShip = true
                    callback(isEnable: true)
                }else{
                    callback(isEnable: false)
                }
            }
        })
    }
    
    func friendshipQuery(callback:(mFriendship:PFObject!)->Void){
        self.friendshipQuery(callback, create: true)
    }
    
    func friendshipQuery(callback:(mFriendship:PFObject!)->Void,create:Bool){
        if (friendship != nil) {
            if self.messageTextView.text != "" {
                self.friendship["lastMessage"] = self.messageTextView.text
                self.friendship["dateLastMessage"] = NSDate()
                self.friendship.saveInBackground()
            }
            callback(mFriendship: friendship)
        }else{
            
            let innerP1 = NSPredicate(format: "fromUser = %@ AND toUser = %@",senderUser,addresseeUser)
            var innerQ1 = PFQuery(className: "friendship", predicate: innerP1)
            
            let innerP2 = NSPredicate(format: "fromUser = %@ AND toUser = %@",addresseeUser,senderUser)
            var innerQ2 = PFQuery(className: "friendship", predicate: innerP2)
            
            var query = PFQuery.orQueryWithSubqueries([innerQ1,innerQ2])
            query.addAscendingOrder("createdAt")
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                if var objs = objects as? [PFObject] {
                    if objs.count > 0 {
                        self.friendship = objs[0]
                        if self.messageTextView.text != "" {
                            self.friendship["lastMessage"] = self.messageTextView.text
                            self.friendship["dateLastMessage"] = NSDate()
                            self.friendship.saveInBackground()
                        }
                        
                        callback(mFriendship: self.friendship)
                    }else{
                        if(create){
                            var myFriendship:PFObject = PFObject(className: "friendship")
                            var groupACL = PFACL()
                            groupACL.setReadAccess(true, forUser:self.senderUser)
                            groupACL.setWriteAccess(true, forUser:self.senderUser)
                            groupACL.setReadAccess(true, forUser:self.addresseeUser)
                            groupACL.setWriteAccess(true, forUser:self.addresseeUser)
                        
                            myFriendship.ACL = groupACL
                            myFriendship["fromUser"] = self.senderUser
                            myFriendship["toUser"] = self.addresseeUser
                            myFriendship["status"] = "pending"
                            if self.messageTextView.text != "" {
                                myFriendship["lastMessage"] = self.messageTextView.text
                                self.friendship["dateLastMessage"] = NSDate()
                            }
                            myFriendship["newFrom"] = true
                            myFriendship.saveInBackgroundWithBlock({
                                succeeded, error in
                                    if succeeded {
                                        self.friendship = myFriendship
                                        callback(mFriendship: self.friendship)

                                    }
                            })
                        }else{
                            callback(mFriendship: nil)
                        }
                        
                    }
                
                }
            }
        }
    
    }
    func getMessageFunc(){
        refreshResult()
    }
    
    @IBAction func goToInformation(sender: AnyObject) {
        self.performSegueWithIdentifier("goToInfo", sender: self)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goToInfo") {
            var svc = segue.destinationViewController as! InformationViewController;
            svc.addresseeUser = self.addresseeUser
            svc.user = self.senderUser
            svc.ourFriendship = self.friendship
            
        }
    }
    func loadData(){
        var imageVar : PFFile!
        if let image:PFFile = addresseeUser.valueForKey("profilePicture") as? PFFile {
            image.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        let image = UIImage(data:imageData)
                        self.photo = image!
                        self.loadImage()
                    }
                }
            }
            imageVar = image
        }
        
        userNameText = self.addresseeUser.valueForKey("username") as? String
     
        var calendar: NSCalendar = NSCalendar.currentCalendar()
         self.userName.text =  "\(self.userNameText), 24:00"
        self.friendshipQuery({
            (mFriendship:PFObject!)->Void in
            if mFriendship != nil {
                var updateTimer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: "updateTime", userInfo: nil, repeats: true)
                self.friendship = mFriendship
                if imageVar != nil {    
                    imageVar.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        if error == nil {
                            if let imageData = imageData {
                                self.updateTime()
                            }
                        }
                    }
                }

                mFriendship.fetch()
                if (mFriendship.valueForKey("fromUser") as! PFUser).objectId == self.senderUser.objectId {
                    mFriendship["newTo"] = false
                }else{
                    mFriendship["newFrom"] = false
                }
                mFriendship.saveInBackground()

            }
        },create: false)
       
        // Replace the hour (time) of both dates with 00:00
        
    }
    func updateTime(){
        let date1 = NSDate()
        if let date2 = self.friendship.valueForKey("acceptedAt") as? NSDate {
            let isPlus =  PFUser.currentUser()?.valueForKey("isPlus") as! Bool
            self.userName.text =  "\(self.userNameText), \(getTimeDown(date1, date2, isPlus))"
            self.loadImage(getTimeDownSize(date1, date2, isPlus))
        }else{
            self.userName.text =  "\(self.userNameText), 24:00"
            self.loadImage(10)
        }
    }
    func loadUIConfiguration(){
        let frameRatio:CGFloat = 1.0
        let infobarRatio:CGFloat = 0.1364
        let viewWidht = view.frame.size.width
        let viewHeight = view.frame.size.height
        let frameWidth:CGFloat = 54.0
        let frameHeight:CGFloat = 54.0
        let userImageWidth = 46
        self.frameImage.frame = CGRectMake(0,0,frameWidth,frameHeight)
        self.frameImage.center = CGPointMake(viewWidht/2, 42.0)
        userImage.frame = CGRectMake(0,0,frameWidth-4,frameWidth-2)
        userImage.center = CGPointMake((viewWidht/2)-1, 42.0)
        //        userImage.layer.zPosition = 2
        userImage.clipsToBounds = true
        userImage.contentMode = .ScaleAspectFill
//        let infobarHeight = frameWidth *  infobarRatio
//        let infobarY = frameHeight + infobarHeight + 63
//        infobar.frame = CGRectMake(CGFloat(26), infobarY, frameWidth, infobarHeight)
//        
//        name.frame = CGRectMake(40, infobarY + 5 ,170, 21)
//        name.layer.zPosition = 2
      
    }
    func loadImage(){
        self.loadImage(10)
    }

    func loadImage(size:CGFloat){
        
        let width = photo.size.width
        let height = photo.size.height
        let ratio = photo.size.width/photo.size.height
        var newImgMin = resizeImage(photo, CGSize(width: size, height: size / ratio))
        var newImg:UIImage? = resizeImage(newImgMin, CGSize(width: width, height: height))
        userImage.image = newImg
        
        
    }
    
    @IBAction func backBtnClick(sender: AnyObject) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController!.navigationBar.tintColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func didTapScrollView(){
        self.view.endEditing(true)
    }
    
    func textViewDidChange(textView: UITextView) {
        if !messageTextView.hasText(){
            self.messagePlaceholeder.hidden = false
        }else{
            self.messagePlaceholeder.hidden = true
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if !messageTextView.hasText(){
            self.messagePlaceholeder.hidden = false
        }
    }
    
    func keyboardWillHide(notification:NSNotification){
        let dict:NSDictionary = notification.userInfo!
        let s:NSValue = dict.valueForKey(UIKeyboardFrameBeginUserInfoKey) as! NSValue!
        let rect:CGRect = s.CGRectValue()
        UIView.animateWithDuration(0.3, delay: 0, options: .CurveLinear, animations: {
            //            self.scrollView.frame.origin.y = self.scrollViewOriginalY
            self.frameMessageView.frame.origin.y = self.frameMessageOriginalY
            self.scrollView.frame.size.height = self.scrollView.frame.height + rect.height
            }, completion: {
                (finished:Bool) in
                
        })
    }
    
    
    func keyboardWillShow(notification:NSNotification){
        let dict:NSDictionary = notification.userInfo!
        let s:NSValue = dict.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue!
        //let rect:CGRect = s.CGRectValue()
        let rect:CGRect = self.view.convertRect(s.CGRectValue(), fromView: nil)
        
        
        UIView.animateWithDuration(0.3, delay: 0, options: .CurveLinear, animations: {
            var newHeight = (UIScreen.mainScreen().bounds.height - self.scrollView.frame.origin.y) - rect.height - 50
            println(newHeight)
            self.scrollView.frame.size.height = newHeight
            self.frameMessageView.frame.origin.y = self.frameMessageOriginalY - rect.height
            
            if self.scrollView.contentSize.height >= self.scrollView.bounds.size.height {
                var bottomOffset:CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height )
                self.scrollView.setContentOffset(bottomOffset, animated: false)
            } else {
                var bottomOffset:CGPoint = CGPointMake(0, 0)
                self.scrollView.setContentOffset(bottomOffset, animated: false)
            }
            }, completion: {
                (finished:Bool) in
                
            })
    }
    
    
    
    func publishMessage(){
    
        
            var messageObject = PFObject(className: "Messages")
            messageObject["sender"] = senderUser
            messageObject["addressee"] = addresseeUser
            messageObject["message"] = messageTextView.text
  
            messageObject["friendship"] = friendship
            var groupACL = PFACL()
            groupACL.setReadAccess(true, forUser:self.senderUser)
            groupACL.setWriteAccess(true, forUser:self.senderUser)
            groupACL.setReadAccess(true, forUser:self.addresseeUser)
            groupACL.setWriteAccess(true, forUser:self.addresseeUser)
        
            messageObject.ACL = groupACL
            friendship.fetchInBackgroundWithBlock({
                friendship, error in
                self.friendship = friendship
                if error != nil{
                    if (self.friendship.valueForKey("fromUser") as! PFUser).objectId == self.senderUser.objectId {
                        self.friendship["newFrom"] = true
                    }else{
                        self.friendship["newTo"] = true
                    }
                    
                    self.friendship.saveEventually()
                }
            })
        
        
            var bubble = self.createMessage(messageObject,createdAt: NSDate(), type: "aqua",x: self.messageX,y: self.messageY)
            bubble.alpha = 0.75
            self.scrollView.addSubview(bubble)
            self.messageY += bubble.frame.size.height + 3
            self.scrollView.contentSize  = CGSizeMake(self.view.frame.size.width, self.messageY )
            self.messageTextView.text = ""
            self.messagePlaceholeder.hidden = false
//            var bottomOffset:CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height )
//            self.scrollView.setContentOffset(bottomOffset, animated: false)
        if self.scrollView.contentSize.height >= self.scrollView.bounds.size.height {
            var bottomOffset:CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height )
            self.scrollView.setContentOffset(bottomOffset, animated: false)
        } else {
            var bottomOffset:CGPoint = CGPointMake(0, 0)
            self.scrollView.setContentOffset(bottomOffset, animated: false)
        }
            messageObject.saveInBackgroundWithBlock({succeeded, error in
                if succeeded{
                    
                    let innerP1 = NSPredicate(format: "userId = %@", self.addresseeUser.valueForKey("objectId") as! String)
                    var query = PFQuery(className: "preferences", predicate: innerP1)
                    
                    query.addAscendingOrder("createdAt")
                    query.findObjectsInBackgroundWithBlock {
                        (objects: [AnyObject]?, error: NSError?) -> Void in
                        if var objs = objects as? [PFObject] {
                            if objs.count > 0 {
                                dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.value), 0)) {
                                    //self.preferencias = objs[0]
                                    var pushQuery:PFQuery = PFInstallation.query()!
                                    pushQuery.whereKey("user", equalTo: self.addresseeUser)
                                    var push:PFPush = PFPush()
                                    push.setQuery(pushQuery)
                                    let dicData : NSMutableDictionary = NSMutableDictionary()
                                
                                    if (objs[0].valueForKey("messages") != nil) {
                                        if objs[0].valueForKey("messages") as! Bool {
                                            dicData.setValue(NSLocalizedString("Blaze - Nuevo mensaje de ", comment: "") + self.senderUser.username!, forKey: "alert")
                                            // push.setMessage("Blaze - Nuevo mensaje de " + self.senderUser.username!)
                                            if (objs[0].valueForKey("vibraciones") != nil) {
                                                if objs[0].valueForKey("vibraciones") as! Bool {
                                                    dicData.setValue("default", forKey: "sound")
                                                } else {
                                                }
                                            } else {
                                                dicData.setValue("default", forKey: "sound")
                                            }
                                        } else {
                                        }
                                    } else {
                                        dicData.setValue(NSLocalizedString("Blaze - Nuevo mensaje de ", comment: "") + self.senderUser.username!, forKey: "alert")
                                        if (objs[0].valueForKey("vibraciones") != nil) {
                                            if objs[0].valueForKey("vibraciones") as! Bool {
                                                dicData.setValue("default", forKey: "sound")
                                            } else {
                                            }
                                        } else {
                                            dicData.setValue("default", forKey: "sound")
                                        }
                                    }
                                
                                    let data = NSDictionary(dictionary: dicData)
                                
                                    push.setData(data as [NSObject : AnyObject])
                                    push.sendPush(nil)
                                    self.messageCount++
                                    println("mensaje enviado")
                                }
                                bubble.alpha = 1
                            }
                        }
                    }
                    
                }else{
                    messageObject.saveEventually();
                    JLToast.makeText(NSLocalizedString("Error al enviar el mensaje", comment: "")).show()
                }
            })
        
    }
    @IBAction func sendMessage(sender: AnyObject) {
        if !messageTextView.hasText() {
            println("no text")
        }else{
            self.checkRelation({
                isEnable in
                if isEnable {
                    self.publishMessage()
                }else{
                    if self.messageCount > 0 {
                        JLToastView.setDefaultValue(CGFloat(300), forAttributeName: JLToastViewPortraitOffsetYAttributeName, userInterfaceIdiom: .Phone)
                        var jToast = JLToast.makeText(NSLocalizedString("Esta pendiente la solicitud de chat", comment: ""))
                        jToast.show()
                    }else{
                        self.publishMessage()
                    }
                }
            })
        }
        
    }
    func refreshResult(){
        messageArray.removeAll(keepCapacity: false)
        
        
        let innerP1 = NSPredicate(format: "sender = %@ AND addressee = %@",senderUser,addresseeUser)
        var innerQ1 = PFQuery(className: "Messages", predicate: innerP1)
        
        let innerP2 = NSPredicate(format: "sender = %@ AND addressee = %@",addresseeUser,senderUser)
        var innerQ2 = PFQuery(className: "Messages", predicate: innerP2)
        
        var query = PFQuery.orQueryWithSubqueries([innerQ1,innerQ2])
        query.addAscendingOrder("createdAt")
        query.limit = 1000
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                self.scrollView.subviews.map({ $0.removeFromSuperview() })
                var objectList:[PFObject] = objects as! [PFObject]
                var messageSender:PFUser
                for object in objectList {
                    messageSender = object.objectForKey("sender") as! PFUser
                    
                    var bubble:UIImageView!
                    if messageSender.objectId == self.senderUser.objectId {
                        bubble = self.createMessage(object,createdAt: object.createdAt!, type: "aqua",x: self.messageX,y: self.messageY)
                        self.scrollView.addSubview(bubble)
                       
                    }else{
                        bubble = self.createMessage(object,createdAt: object.createdAt!, type: "orange",x: self.messageX,y: self.messageY)
                        self.scrollView.addSubview(bubble)
                        
                        
                    }
                    self.messageY += bubble.frame.size.height + 3
                    self.scrollView.contentSize  = CGSizeMake(self.view.frame.size.width, self.messageY )
                    
                    self.messageCount++
//                    var bottomOffset:CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height )
//                    self.scrollView.setContentOffset(bottomOffset, animated: false)
                    if self.scrollView.contentSize.height >= self.scrollView.bounds.size.height {
                        var bottomOffset:CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height )
                        self.scrollView.setContentOffset(bottomOffset, animated: false)
                    } else {
                        var bottomOffset:CGPoint = CGPointMake(0, 0)
                        self.scrollView.setContentOffset(bottomOffset, animated: false)
                    }
                }
                
            } else {
                 JLToast.makeText(NSLocalizedString("Ha ocurrido un error al traer las conversaciones", comment: "")).show()
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
    }
    
    func createMessage(object:PFObject,createdAt:NSDate, type:String,x:CGFloat,y:CGFloat)->UIImageView! {
        var myTxt = object.objectForKey("message") as! String!
        
        var date:NSDate = createdAt
        let hour = formatter.stringFromDate(date)
        
        var hourView = UILabel()
        hourView.frame = CGRectMake(0,0,60,20)
        hourView.textAlignment = NSTextAlignment.Left
        hourView.font = UIFont(name: "Helvetica Neuse", size: 10)
        hourView.font = hourView.font.fontWithSize(10)
        hourView.textColor = UIColor.darkGrayColor()
        hourView.text = hour
        
        var messageLbl:UILabel = UILabel()
        messageLbl.frame = CGRectMake(0,0,self.scrollView.frame.size.width-70,10)
        messageLbl.backgroundColor = UIColor.clearColor()
        messageLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        messageLbl.textAlignment = NSTextAlignment.Left
        messageLbl.numberOfLines = 0
        messageLbl.font = UIFont(name: "Helvetica Neuse", size: 17)
        messageLbl.textColor = UIColor.blackColor()
        messageLbl.text = myTxt
        messageLbl.layer.zPosition = 30
        messageLbl.frame.origin.x = 25
        messageLbl.frame.origin.y = 7
        var newSize = messageLbl.sizeOfString(self.scrollView.frame.size.width-30)
        messageLbl.frame.size.height = newSize.height
        
        var myImage = UIImage(named: type)!
        let myInsets : UIEdgeInsets = UIEdgeInsetsMake(10, 20, 20, 20)
        myImage = myImage.resizableImageWithCapInsets(myInsets)
        
        var bubble = UIImageView()
        bubble.image = myImage
        var bubbleH = newSize.width + 23 > self.scrollView.frame.size.width-60 ? newSize.height+32 : newSize.height+24

        bubble.frame = CGRectMake(x,y, self.scrollView.frame.size.width-30, bubbleH)
        hourView.frame.origin.x = newSize.width + 23 > self.scrollView.frame.size.width-60 ? 27 : newSize.width + 27
        hourView.frame.origin.y = newSize.width + 23 > self.scrollView.frame.size.width-60 ? newSize.height + 3 : newSize.height - 10
        
        bubble.addSubview(hourView)
        bubble.addSubview(messageLbl)
        return bubble
    }
    
}

