//
//  FriendsCollectionViewCell.swift
//  blaze
//
//  Created by devmg on 19/8/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import UIKit
import Parse

class FriendsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var frameImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    var photo:UIImage!
    var user:  PFUser!{
        didSet {
            updateUI()
        }
    }
    
    func updateUI(){
        userImage.frame = CGRectMake(CGFloat(9),CGFloat(4),49,51)
        //        userImage.layer.zPosition = 2
        userImage.clipsToBounds = true
        userImage.contentMode = .ScaleAspectFill
        userName.text = user.username
        var image:PFFile = user.valueForKey("profilePicture") as! PFFile
        image.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil {
                if let imageData = imageData {
                    let image = UIImage(data:imageData)
                    self.photo = image!
                    self.loadImage()
                }
            }
        }
    }
    func loadImage(){
        
        let width = photo.size.width
        let height = photo.size.height
        let ratio = photo.size.width/photo.size.height
        var newImgMin = resizeImage(photo, CGSize(width: 10, height: 10 / ratio))
        var newImg:UIImage? = resizeImage(newImgMin, CGSize(width: width, height: height))
        userImage.image = newImg
        
        
    }
}
