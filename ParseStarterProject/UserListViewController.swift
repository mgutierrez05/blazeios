//
//  UserListViewController.swift
//  blaze
//
//  Created by devmg on 6/7/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import UIKit
import Parse

class UserListViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, ScrollControlProtocol {
    
    var pageViewController: UIPageViewController!
    
    var pageControl: UIPageControl!
    
    var arrayInformationMessages = [PFObject]()
    
    var arrayViewControllers: [UIViewController] = []
    
    let transitionManager = TransitionManager()
    var logo:UIImage!
    var logoIV:UIImageView!
    var currentRadius:CGFloat = 180.0
    var chatBarButton:UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadToolBar()
        loadLogoPull()
        // Array containing UIViewControllers which are in fact under the hood
        // downcasted ContentViewControllers.
        self.initArrayViewControllers()
        
        // UIPageViewController initialization and configuration.
        let toolbarHeight: CGFloat = 60.0
        self.initPageViewController(toolbarHeight)
        
        // Retrieving UIPageControl
        self.initPageControl()
        self.transitionManager.sourceViewController = self
        checkNewMessage()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getMessageFunc", name: "getMessage", object: nil)
        
        if (NSUserDefaults.standardUserDefaults().valueForKey("goToPush") != nil) {
            if NSUserDefaults.standardUserDefaults().valueForKey("goToPush") as! Bool {
                self.performSegueWithIdentifier("goToChats", sender: self)
                NSUserDefaults.standardUserDefaults().removeObjectForKey("goToPush")
                NSUserDefaults.standardUserDefaults().synchronize()
            }
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if (NSUserDefaults.standardUserDefaults().valueForKey("reloadSearch") != nil){
            if NSUserDefaults.standardUserDefaults().valueForKey("reloadSearch") as! Bool {
                NSUserDefaults.standardUserDefaults().removeObjectForKey("reloadSearch")
                NSUserDefaults.standardUserDefaults().synchronize()
                self.performSegueWithIdentifier("goToLoadingAnim", sender: self)
            }
        }

    }
    
    override func viewWillAppear(animated: Bool) {
        checkNewMessage()
    }
    
    func getMessageFunc(){
        checkNewMessage()
    }
    func loadLogoPull(){
        
        logoIV = UIImageView(frame: CGRect(x: 0, y: 0, width: 41, height: 66))
        logoIV.animationImages = [UIImage]()
        for var index = 1; index < 5; index++ {
            var frameName = "logoanim\(index)"
            logoIV.animationImages?.append(UIImage(named: frameName)!)
        }
        logoIV.animationDuration = 1.5
        logoIV.contentMode = .ScaleAspectFit
        logoIV.layer.zPosition = 20
        let x = CGFloat(view.frame.width/2)
        let y = CGFloat(130)
        logoIV.center = CGPointMake(x,y)
        logoIV.startAnimating()
        logoIV.alpha = 0.0
        
        view.addSubview(logoIV)
    }
    func showLogoAnim(perc:CGFloat){
        logoIV.alpha = perc < 0.30 ? 0 : perc - 0.25
    }
    func fadeOutLogoAnim(){
        UIView.animateWithDuration(0.15, animations: {
            self.logoIV.alpha = 0
            
        })
    }
    func goToChat(sender: UIBarButtonItem) {
    }
    
    func loadToolBar(){
        navigationController?.setNavigationBarHidden(false, animated: true)
        let logo = UIImage(named: "blazeicon.png")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 43))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = logo
        self.navigationItem.titleView = imageView
        
        self.navigationController!.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.tintColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        self.navigationItem.hidesBackButton = true
        let config = UIImage(named: "config.png")
        let chat = UIImage(named: "chat.png")
        
        var configBarButton = UIBarButtonItem(image: config, style: .Plain , target: self, action: "goToConfig")
        chatBarButton = UIBarButtonItem(image: chat, style: .Done, target: self, action: "goToChat")
//        chatBarButton.tintColor = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        self.navigationItem.leftBarButtonItem = configBarButton
        self.navigationItem.rightBarButtonItem = chatBarButton
        self.navigationController?.navigationBar.frame.origin.y = 35
        self.navigationController!.navigationBar.clipsToBounds = true;
        
    }

    func checkNewMessage(){
        let innerP1 = NSPredicate(format: "fromUser = %@ AND newTo = %@ AND status = %@",PFUser.currentUser()!,true,"accepted")
        var innerQ1 = PFQuery(className: "friendship", predicate: innerP1)
        
        let innerP2 = NSPredicate(format: "toUser = %@ AND newFrom = %@ ",PFUser.currentUser()!,true)
        var innerQ2 = PFQuery(className: "friendship", predicate: innerP2)
        
        var query = PFQuery.orQueryWithSubqueries([innerQ1,innerQ2])
        query.addAscendingOrder("updatedAt")
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                if let obs = objects as? [PFObject]  {
                    if obs.count == 0 {
                        self.colorNoMessage()
                        println(obs.count)
                    }else{
                         println(obs.count)
                        self.colorNewMessage()
                    }
                    
                }
                
             
            } else {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
    }
    func colorNewMessage(){
        chatBarButton.tintColor = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    }
    func colorNoMessage(){
        chatBarButton.tintColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    }
    func goToChat(){
         self.performSegueWithIdentifier("goToChats", sender: self)
        
    }
    
    func goToConfig(){
//        PFFacebookUtils.session()?.closeAndClearTokenInformation()
//        PFUser.logOut()
//        navigationController?.setNavigationBarHidden(true, animated: true)
//        self.navigationController!.popToRootViewControllerAnimated(false)
        
        /*
        CATransition *transition = [CATransition animation];
        transition.duration = 0.45;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
        transition.type = kCATransitionFromLeft;
        [transition setType:kCATransitionPush];
        transition.subtype = kCATransitionFromLeft;
        transition.delegate = self;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:pageView animated:NO];
        */
        
        self.performSegueWithIdentifier("goToConfig", sender: self)
    }

    
    // Required UIPageViewControllerDataSource method
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index: Int = self.arrayInformationMessages.count == 0 ? 0 : (viewController as! UserItemViewController).index
        if index == NSNotFound {
            return nil
        }
        
        index++
        
        if (index == self.arrayViewControllers.count) {
            return nil
        }
        
        return self.arrayViewControllers[index]
    }
    
    // Required UIPageViewControllerDataSource method
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index: Int = self.arrayInformationMessages.count == 0 ? 0 : (viewController as! UserItemViewController).index
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index--
        
        return self.arrayViewControllers[index]
    }
    
    // Optional UIPageViewControllerDataSource method
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    // Optional UIPageViewControllerDataSource method
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    /*
    Optional UIPageViewControllerDelegate method
    
    Sent when a gesture-initiated transition ends. The 'finished' parameter indicates whether
    the animation finished, while the 'completed' parameter indicates whether the transition completed or bailed out (if the user let go early).
    */
    func pageViewController(pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [AnyObject],
        transitionCompleted completed: Bool)
    {
        // Turn is either finished or aborted
        if (completed && finished) {
            // let previousViewController = previousViewControllers[0] as ContentViewController
            let currentDisplayedViewController = self.pageViewController!.viewControllers[0] as! UserItemViewController
            self.pageControl.currentPage = currentDisplayedViewController.index
        }
    }
    
    private func initArrayViewControllers() {
        if self.arrayInformationMessages.count == 0 {
            let contentViewController = self.storyboard!.instantiateViewControllerWithIdentifier("NoContentViewControllerID") as! NoUserItemViewController
            self.arrayViewControllers.append(contentViewController)
        }
        for (var i = 0; i < self.arrayInformationMessages.count; i++) {
            let contentViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ContentViewControllerID") as! UserItemViewController
            contentViewController.index = i
            contentViewController.user = self.arrayInformationMessages[i]
            contentViewController.scrollControl = self

            self.arrayViewControllers.append(contentViewController)
        }
    }
    
    private func initPageViewController(let toolbarHeight: CGFloat) {
        //        self.pageViewController = self.storyboard!.instantiateViewControllerWithIdentifier("PagesWidgetID") as! UIPageViewController
        self.pageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.Scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.Horizontal, options: nil)
        
        if let pageViewController = self.pageViewController {
            var initViewController:UIViewController
            if self.arrayInformationMessages.count > 0 {
                initViewController = self.arrayViewControllers[0] as! UserItemViewController
            }else{
                initViewController = self.arrayViewControllers[0] as! NoUserItemViewController
            }
            let vcArray = [initViewController]
            let frame = self.view.frame
            pageViewController.dataSource = self
            pageViewController.delegate = self
            pageViewController.view.frame = CGRectMake(0, 75, frame.width, frame.height-75 )
            pageViewController.setViewControllers(vcArray, direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
            
            self.addChildViewController(pageViewController)
            self.view.addSubview(pageViewController.view)
            //  pageViewController.view.clipsToBounds = true
            pageViewController.didMoveToParentViewController(self) 
        }
    }
    
    func disableScroll(){
        for v in self.pageViewController!.view.subviews {
            if let sub = v as? UIScrollView {
                sub.scrollEnabled = false
            }
        }
    }
    func enableScroll(){
        for v in self.pageViewController!.view.subviews {
            if let sub = v as? UIScrollView {
                sub.scrollEnabled = true
            }
        }
    }
    private func initPageControl() {
        let subviews: Array = self.pageViewController.view.subviews
        var pageControl: UIPageControl! = nil
        
        for (var i = 0; i < subviews.count; i++) {
            if (subviews[i] is UIPageControl) {
                pageControl = subviews[i] as! UIPageControl
                break
            }
        }
        
        self.pageControl = pageControl
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goToLoadingAnim") {
            var svc = segue.destinationViewController as! LoadingViewController;
//            svc.transitioningDelegate = self.transitionManager
            svc.isModal = true
            
        } else if ( segue.identifier == "goToConfig") {
            var sourceViewController = segue.sourceViewController as! UIViewController
            var destinationViewController = segue.destinationViewController as! UIViewController
            
            var transition: CATransition = CATransition()
            
            transition.duration = 0.25
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
            transition.subtype = kCATransitionFromLeft; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
            
            sourceViewController.navigationController?.view.layer.addAnimation(transition, forKey: "kCATransition")
            //sourceViewController.navigationController?.pushViewController(destinationViewController, animated: false)

        }
    }
    func goToLoadingAnim(){
        self.performSegueWithIdentifier("goToLoadingAnim", sender: self)
    }
//    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
//        if let touch =  touches.first as? UITouch {
//            println("touch")
//        }
//       
//    }
//    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
//        if let touch =  touches.first as? UITouch {
//            println("touch m")
//        }
//        
//    }
//    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
//        if let touch =  touches.first as? UITouch {
//            println("touch e")
//        }
//        
//    }
    @IBAction func unwindToMainViewController (sender: UIStoryboardSegue){
        // bug? exit segue doesn't dismiss so we do it manually...
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if(sender.sourceViewController .isKindOfClass(LoadingViewController))
        {   self.view.subviews.map({ $0.removeFromSuperview() })
            self.pageViewController.removeFromParentViewController()
            self.arrayViewControllers.removeAll(keepCapacity: false)
            var view:LoadingViewController = sender.sourceViewController as! LoadingViewController
            self.arrayInformationMessages = view.arrayInformationMessages
            // downcasted ContentViewControllers.
            self.initArrayViewControllers()
            
            // UIPageViewController initialization and configuration.
            let toolbarHeight: CGFloat = 60.0
            self.initPageViewController(toolbarHeight)
            
            // Retrieving UIPageControl
            self.initPageControl()
            loadLogoPull()
            
        }
    }

}