//
//  FBUtil.swift
//  blaze
//
//  Created by devmg on 1/7/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import Parse
class FBUtil {
    
    class func notLoggedIn() -> Bool {
        let user = PFUser.currentUser()
        // here I assume that a user must be linked to Facebook
        return user == nil || !PFFacebookUtils.isLinkedWithUser(user!)
    }
    class func loggedIn() -> Bool {
        return !notLoggedIn()
    }
    
    
    
    class func obtainUserNameAndFbId() {
        if notLoggedIn() {
            return
        }
        let user = PFUser.currentUser() // Won't be nil because is logged in
        // RETURN IF WE ALREADY HAVE A USERNAME AND FBID (note that we check the fbId because Parse automatically fills in the username with random numbers)
        user.
        // REQUEST TO FACEBOOK
        println("performing request to FB for username and IDF...")
        if let session = PFFacebookUtils.session() {
            if session.isOpen {
                println("session is open")
                FBRequestConnection.startForMeWithCompletionHandler({ (connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
                    println("done me request")
                    if error != nil {
                        println("facebook me request - error is not nil :(")
                    } else {
                        println("facebook me request - error is nil :)")
                        println(result)
                        // You have 2 ways to access the result:
                        // 1)
                        println(result["name"])
                        println(result["id"])
                        // 2)
                        println(result.name)
                        println(result.objectID)
                        // Save to Parse:
                        PFUser.currentUser()!.username = result.name
                        PFUser.currentUser()!.setValue(result.objectID, forKey: "fbId")
                        PFUser.currentUser(!).saveEventually({
                        ()
                        }) // Always use saveEventually if you want to be sure that the save will succeed
                    }
                })
            }
        }
    }
    
}