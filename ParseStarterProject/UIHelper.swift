
func LogoRectMake(viewWidth:Double, viewHeight:Double)->CGRect {
    let originalWidth:Double = 744.0 // 1242
    let originalHeight:Double = 909.0
    
    return LoginRectMake(138,originalWidth,originalHeight,viewWidth,viewHeight)
}

func LoginRectMake(originalY:Double,originalWidth:Double,originalHeight:Double,viewWidth:Double, viewHeight:Double)->CGRect{
    let designWidth:Double = 1242
    let designHeight:Double = 2208
    let screenWidth:Double = viewWidth
    let screenHeight:Double = viewHeight
    let newWidht = (originalWidth * screenWidth) / designWidth
    let newMargin = (screenWidth - newWidht)/2
    let newY = (originalY * viewHeight) / designHeight
    let newHeight = (newWidht * originalHeight) / originalWidth
    return CGRectMake(CGFloat(newMargin),CGFloat(newY),CGFloat(newWidht),CGFloat(newHeight))
}
func FBBtnRectMake(viewWidth:Double, viewHeight:Double)->CGRect {
    let originalWidth:Double = 744.0 // 1242
    let originalHeight:Double = 357.0
    
    return LoginRectMake(1191,originalWidth,originalHeight,viewWidth,viewHeight)
}
func DownloadBtnRectMake(viewWidth:Double, viewHeight:Double)->CGRect {
    let originalWidth:Double = 744.0 // 1242
    let originalHeight:Double = 273.0
    
    return LoginRectMake(1731,originalWidth,originalHeight,viewWidth,viewHeight)
}
func resizeImage(image: UIImage, newSize: CGSize) -> (UIImage) {
    let newRect = CGRectIntegral(CGRectMake(0,0, newSize.width, newSize.height))
    let imageRef = image.CGImage
    
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
    let context = UIGraphicsGetCurrentContext()
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh)
    let flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
    
    CGContextConcatCTM(context, flipVertical)
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef)
    
    let newImageRef = CGBitmapContextCreateImage(context) as CGImage
    let newImage = UIImage(CGImage: newImageRef)
    
    // Get the resized image from the context and a UIImage
    UIGraphicsEndImageContext()
    
    return newImage!
}