	//
//  ChatCell.swift
//  blaze
//
//  Created by devmg on 20/8/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import UIKit
import Parse

class ChatCell: UITableViewCell {

    @IBOutlet weak var timer: UILabel!
    @IBOutlet weak var line: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var containerBtns: UIView!
    @IBOutlet weak var rechazarBtn: UIButton!
    @IBOutlet weak var aceptarBtn: UIButton!
    @IBOutlet weak var irChatBtn: UIButton!
    
    var parentTable:UITableView!
    var indexPosition:NSIndexPath!
    var chatController: ChatsViewController!
    var friendship:PFObject!
    var user:PFUser!
    var photo:UIImage!
    var isNewMessage:Bool = false
    
    var originalCenter = CGPoint()
    var originalCenterBtns = CGPoint()
    
    var deleteOnDragRelease = false
   
    var status: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        container.backgroundColor = UIColor.whiteColor()
        container.layer.cornerRadius = 4.0
        container.layer.borderColor = UIColor.grayColor().CGColor
        container.layer.borderWidth = 1.5
        container.clipsToBounds = true
        userImage.frame = CGRectMake(CGFloat(9),CGFloat(11),52,52)
        //        userImage.layer.zPosition = 2
        userImage.clipsToBounds = true
        userImage.contentMode = .ScaleAspectFill
    
        rechazarBtn.hidden = true
        aceptarBtn.hidden = true
        rechazarBtn.layer.zPosition = 10
    
        loadGesture()
    }
    
    func loadGesture(){
        var recognizer = UIPanGestureRecognizer(target: self, action: "handlePan:")
        recognizer.delegate = self
        addGestureRecognizer(recognizer)
    }
    
    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let translation = gestureRecognizer.locationInView(self)
        println("x \(translation.x)")
        println("y \(translation.y)")
        
        if translation.x > 230 || deleteOnDragRelease {
            println("true")
            return true
        }
        println("false")
        return false
    }
    
    //MARK: - horizontal pan gesture methods
    func handlePan(recognizer: UIPanGestureRecognizer) {
        // 1
        if recognizer.state == .Began {
            
            if status != "pending"{
            // when the gesture begins, record the current center location
                originalCenter = container.center
                originalCenterBtns = containerBtns.center
            }
        }
        // 2
        if recognizer.state == .Changed {
             if status != "pending"{
                // has the user dragged the item far enough to initiate a delete/complete?
                let translation = recognizer.translationInView(self)
                if(translation.x < 0){
                    deleteOnDragRelease = container.frame.origin.x < -25
                }else{
                    deleteOnDragRelease = false
                }
                if(!deleteOnDragRelease && translation.x < 15.0){
                    println(translation.x)
                    container.center = CGPointMake(originalCenter.x + translation.x, originalCenter.y)
                    containerBtns.center = CGPointMake(originalCenterBtns.x + translation.x, originalCenterBtns.y)
                
                }
            }
        }
        // 3
        if recognizer.state == .Ended {
             if status != "pending"{
                // the frame this cell had before user dragged it
                let originalFrame = CGRect(x: 8, y: container.frame.origin.y,
                    width: container.bounds.size.width, height: container.bounds.size.height)
                let originalFrameBtns = CGRect(x:bounds.size.width+2, y: containerBtns.frame.origin.y, width:containerBtns.bounds.size.width, height:containerBtns.bounds.size.height)
                
                if !deleteOnDragRelease {
                    // if the item is not being deleted, snap back to the original location
                    UIView.animateWithDuration(0.2, animations: {
                    
                        self.container.frame = originalFrame
                        self.containerBtns.frame = originalFrameBtns
                    })
                }else{
                    UIView.animateWithDuration(0.15, animations: {
                    
                        self.container.center = CGPointMake( 150 - 233.0, self.originalCenter.y)
                        self.containerBtns.center = CGPointMake(self.bounds.size.width + 100 - 233.0, self.originalCenterBtns.y)
                    })
            
                }
            }
        }
    }
    //UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    func loadData(){
        line.text = friendship.valueForKey("lastMessage") as? String
        status = friendship.valueForKey("status") as? String
        if status == "pending" {
            rechazarBtn.hidden = false
            aceptarBtn.hidden = false
            
        }else{
        
        }
        if isNewMessage {
            
            container.layer.borderColor = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 53.0/255.0, alpha: 1.0).CGColor
        }else{
            container.layer.borderColor = UIColor.grayColor().CGColor
        }
        user.fetchInBackground()
        println(user.valueForKey("objectId"))
        if (user.valueForKey("profilePicture") != nil) {
            var image:PFFile = user.valueForKey("profilePicture") as! PFFile
            image.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        let image = UIImage(data:imageData)
                        self.photo = image!
                        if let dateAcepted = self.friendship.valueForKey("acceptedAt") as? NSDate {
                            self.timer.text = getTimeDown(NSDate(), dateAcepted, PFUser.currentUser()?.valueForKey("isPlus") as! Bool)
                            self.loadImage(getTimeDownSize(NSDate(), dateAcepted, PFUser.currentUser()?.valueForKey("isPlus") as! Bool))
                        }else{
                            self.timer.text = "24:00"
                            self.loadImage(10)
                        }
                        
                    }
                }
            }
        }
    }
    
    
    func loadImage(size:CGFloat){
        
        let width = photo.size.width
        let height = photo.size.height
        let ratio = photo.size.width/photo.size.height
        var newImgMin = resizeImage(photo, CGSize(width: size, height: size / ratio))
        var newImg:UIImage? = resizeImage(newImgMin, CGSize(width: width, height: height))
        userImage.image = newImg
        
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func acceptChat(sender: AnyObject) {
        self.friendship["acceptedAt"] = NSDate()
        self.friendship["status"] = "accepted"
        self.status = "accepted"
        self.friendship.saveInBackground()
        self.rechazarBtn.hidden = true
        self.aceptarBtn.hidden = true
        
        
    }
    func removeChat(){
        var query = PFQuery(className:"Messages")
        query.whereKey("friendship", equalTo:friendship)
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                if let objects = objects as? [PFObject] {
                    for object in objects {
                        object.deleteInBackground()
                    }
                    
                    
                    self.friendship.deleteInBackground()
                    
                }
            }
        }
        self.parentTable.beginUpdates()
        self.chatController.friendshipArray.removeAtIndex(self.indexPosition.item)
        self.chatController.usrArray.removeAtIndex(self.indexPosition.item)
        let indexPathForRow = NSIndexPath(forItem: self.indexPosition.item, inSection: 0)
        self.parentTable.deleteRowsAtIndexPaths([indexPathForRow], withRowAnimation: UITableViewRowAnimation.Fade)
        self.parentTable.endUpdates()
    }
    @IBAction func refuseChat(sender: AnyObject) {
        removeChat()
    }
    
    @IBAction func goToChat(sender: AnyObject) {
        self.chatController.goToMessage(user)
    }
    @IBAction func borrarChat(sender: AnyObject) {
        removeChat()
    }
    
    @IBAction func bloquearChat(sender: AnyObject) {
        var myUser = PFUser.currentUser()
        if let objId = user.objectId{
            myUser?.addUniqueObject(objId, forKey: "blockedUsers")
            myUser?.saveInBackground()
            removeChat()
        }

    }

    
}
