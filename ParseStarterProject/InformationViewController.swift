// The MIT License (MIT)
//
// Copyright (c) 2015
// King-Wizard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import UIKit
import Parse

class InformationViewController: UIViewController,UIScrollViewDelegate {
    // MARK: - Outlets
    var scrollView = UIScrollView()
    @IBOutlet weak var frame: UIImageView!
//    @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var userImage2: UIImageView!
//    @IBOutlet weak var userImage3: UIImageView!
    var userImage = UIImageView()
    var userImage2 = UIImageView()
    var userImage3 = UIImageView()
    @IBOutlet weak var infobar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numberPhotos: UILabel!
    @IBOutlet weak var lblnoAmigos: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UITextView!
    @IBOutlet weak var timerContainer: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var hsLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    // MARK: - Vars
    var user:PFUser!
    var addresseeUser:PFUser!
    var ourFriendship:PFObject!
    var pageControl: UIPageControl!
    
    var pageImage1 : UIImageView!
    var pageImage2 : UIImageView!
    var pageImage3 : UIImageView!
    
    var index: Int = 0
    //var photo:UIImage!
    var photos = NSMutableArray()
    var scrollControl:UserListViewController!
    
    var locationStart:CGFloat = 0
    var locationEnd:CGFloat = 0
    var originalCenter = CGPoint()
    
    var friendsArr:[PFUser] = [PFUser]()
    var friendsFacebook = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        
//        var downloadQueue :dispatch_queue_t = dispatch_queue_create("callListSesion", nil)
//        
//        var spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
//        spinner.center = CGPointMake(UIScreen.mainScreen().applicationFrame.size.width/2.0, UIScreen.mainScreen().applicationFrame.size.height/2.0)
//        spinner.color = UIColor.blackColor()
//        self.view.addSubview(spinner)
//        spinner.startAnimating()
         self.loadData()
        self.loadNavBar()
        self.loadUIConfiguration()
        self.setImageScale()
        self.loadCommomFirendsFacebook()
//        
//        dispatch_async(downloadQueue, {
//            
//
//            dispatch_async(dispatch_get_main_queue(), {
//                spinner.stopAnimating()
//            })
//        })
    }
    
    override func viewDidLayoutSubviews() {
        
        println(numberPhotos.frame)
        let heightConstraintPrivacidad = NSLayoutConstraint(item: descriptionLbl, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 46)
        view.addConstraint(heightConstraintPrivacidad)
        
        //        if UIScreen.mainScreen().bounds.size.height == 480 {
        //
        //        }
    }
    
    func setImageScale(){
        
        setImageScale(userImage);
        setImageScale(userImage2);
        setImageScale(userImage3);
        
    }
    
    func setImageScale(iv : UIImageView){
        iv.clipsToBounds = true
        iv.contentMode = .ScaleAspectFill
    }
    
    func loadCommomFirendsFacebook(){
        // Get List Of Friends
        myFriends = PFUser.currentUser()?.valueForKey("friends_facebook") as? NSMutableArray
        yourFriends = addresseeUser.valueForKey("friends_facebook") as? NSMutableArray
        //yourFriends = PFUser.currentUser()?.valueForKey("friends_facebook") as? NSMutableArray
        matchFrieds()
//        var completionHandler = { connection, result, error in
//              //  println(result)
//            if result != nil {
//                if result.objectForKey("data")!.count > 0 {
//                    self.myFriends = result["data"] as! NSMutableArray
//                    self.matchFrieds()
//                }
//            }
//            
//            } as FBRequestHandler;
//        
//        println(addresseeUser.valueForKey("username"))
//        var parameterts = ["fields" : "name,installed,first_name,picture"]
//        FBRequestConnection.startWithGraphPath("me/friends/", parameters: parameterts, HTTPMethod: "GET", completionHandler: completionHandler)
//        
//        var completionHandlerYou = { connection, result, error in
//            println(error)
//            println("Entro")
//            println(result)
//            if result != nil {
//                if result.objectForKey("data")!.count > 0 {
//                    self.yourFriends = result["data"] as! NSMutableArray
//                    self.matchFrieds()
//                }
//            }
//        } as FBRequestHandler;
//        
//        println(addresseeUser.valueForKey("username"))
//        var id_Search = addresseeUser["id_facebook"] as! String
//        //id_Search = "10206685462388961"
//        println(id_Search)
//        FBRequestConnection.startWithGraphPath("\(id_Search)/friends/", parameters: parameterts, HTTPMethod: "GET", completionHandler: completionHandlerYou)
        
    }
    
    var myFriends: NSMutableArray!
    var yourFriends: NSMutableArray!
    
    func matchFrieds(){
        if myFriends != nil && yourFriends != nil {
            for var i = 0; i < myFriends.count; i++ {
                let dicMyFriend = myFriends.objectAtIndex(i)
                for var j = 0; j < yourFriends.count; j++ {
                    let dicYouFriend = yourFriends.objectAtIndex(j)
                    if dicMyFriend.objectForKey("id") as! String == dicYouFriend.objectForKey("id") as! String {
                        friendsFacebook.addObject(dicMyFriend)
                    }
                }
            }
            //{"facebook":{"access_token": "919238954812873"}}
            if friendsFacebook.count > 0 {
                self.collectionView.reloadData()
            }
        }
    }
    
    func loadCommomFriends(){
        getCommomIds({
            commomUsers in
            self.friendsArr = commomUsers
            self.collectionView.reloadData()
        })
    }
    
    func getCommomIds(callback:(commomUsers:[PFUser])->Void){
        obtainFriends(addresseeUser, userNotEqual: user, callback: {
            addresseeFriends in
            self.obtainFriends(self.user, userNotEqual: self.addresseeUser, callback: {
                myFriends in
                var commom = addresseeFriends.filter({
                    u in
                    var b = contains(myFriends) { $0.objectId == u.objectId }
                    return b
                })
                callback(commomUsers: commom)
            })
        })
    
    }
    
    func obtainFriends(userToFind:PFUser,userNotEqual:PFUser, callback:(users:[PFUser])->Void){
        var innerQ1 = PFQuery(className: "friendship")
        innerQ1.whereKey("fromUser", equalTo: userToFind)
        innerQ1.whereKey("status", equalTo: "acepted")
        
        var innerQ2 = PFQuery(className: "friendship")
        innerQ2.whereKey("toUser", equalTo: userToFind)
        innerQ2.whereKey("status", equalTo: "acepted")
        
        var query = PFQuery.orQueryWithSubqueries([innerQ1,innerQ2])
        var userFriends = [PFUser]()
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if var objectsPF = objects as? [PFObject]{
                for object in objectsPF {
                    println(object["username"])
                    var fromUser = object.objectForKey("fromUser") as! PFUser
                    var toUser = object.objectForKey("toUser") as! PFUser
                    if fromUser.objectId == userToFind.objectId{
                        if toUser.objectId != userNotEqual.objectId {
                            userFriends.append(toUser)
                        }
                        
                    }else{
                        if fromUser.objectId != userNotEqual.objectId {
                            userFriends.append(fromUser)
                        }
                        
                    }
                }
        
                callback(users: userFriends)
            }
            
            
        }
    }
    
    func loadNavBar(){
        var fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
        fixedSpace.width = 10.0
        
        var fixedSpace2:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
        fixedSpace2.width = 60.0
        
        
        self.navigationController!.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.tintColor = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true
        var okBtn = UIBarButtonItem(title: "OK", style: .Plain , target: self, action: "goBack")
        let more = UIImage(named: "infomore")
        var moreBtn = UIBarButtonItem(image: more, style: .Plain , target: self, action: nil)
        self.navigationItem.leftBarButtonItems = [fixedSpace,okBtn]
        //self.navigationItem.rightBarButtonItems = [moreBtn]
        let logo = UIImage(named: "chattoplogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 23))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = logo
        
        let viewPage = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 15))
        //viewPage.backgroundColor = UIColor.blackColor()
        
        pageImage1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        pageImage2 = UIImageView(frame: CGRect(x: 20, y: 0, width: 15, height: 15))
        pageImage3 = UIImageView(frame: CGRect(x: 40, y: 0, width: 15, height: 15))
        
        pageImage1.image = UIImage(named: "slide-on")
        pageImage2.image = UIImage(named: "slide-off")
        pageImage3.image = UIImage(named: "slide-off")
        
        viewPage.addSubview(pageImage1)
        viewPage.addSubview(pageImage2)
        viewPage.addSubview(pageImage3)
    
        pageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: 40, height: 23))
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
        pageControl.pageIndicatorTintColor = UIColor.grayColor()
        pageControl.currentPageIndicatorTintColor = UIColor.redColor()
        self.navigationItem.titleView = viewPage
        
    }
    
    func goBack(){
         navigationController?.setNavigationBarHidden(true, animated: false)
         self.navigationController?.popViewControllerAnimated(true)
    }
    
    func loadUIConfiguration(){
        let frameRatio:CGFloat = (254/266)
        let infobarRatio:CGFloat = 0.1364
        let viewWidht = view.frame.size.width
        let viewHeight = view.frame.size.height
        let frameWidth = viewWidht - (viewWidht * 0.20)
        let frameHeight = frameWidth * frameRatio
        let userImageWidth = (932 * viewWidht) / 1242
        var frameX:CGFloat = (viewWidht * 0.20)/2
        frame.frame = CGRectMake(frameX,CGFloat(65),frameWidth,frameHeight)
        println(frameWidth)
        //userImage.clipsToBounds = true
        //userImage.contentMode = .ScaleAspectFill
        let infobarHeight = frameWidth *  infobarRatio
        let infobarY = frameHeight + infobarHeight + 28
        infobar.frame = CGRectMake(frameX, infobarY, frameWidth, infobarHeight)
        distanceLbl.frame.origin.x = frameX + 5
        descriptionLbl.frame.origin.x = frameX
        
        var i = 0
        if (addresseeUser.valueForKey("profilePicture") != nil) {
            i++
        }
        if (addresseeUser.valueForKey("pictureLeft") != nil) {
            i++
        }
        if (addresseeUser.valueForKey("pictureRight") != nil) {
            i++
        }
        
        if i == 1 {
            pageImage1.hidden = true
            pageImage2.hidden = false
            pageImage3.hidden = true
            pageImage2.image = UIImage(named: "slide-on")
        } else if i == 2{
            pageImage1.hidden = false
            pageImage2.hidden = false
            pageImage3.hidden = true
            
            pageImage1.frame = CGRect(x: 13, y: 0, width: 15, height: 15)
            pageImage2.frame = CGRect(x: 33 , y: 0, width: 15, height: 15)
            
        } else if i == 3 {
            pageImage1.hidden = false
            pageImage2.hidden = false
            pageImage3.hidden = false
        } else {
            pageImage1.hidden = true
            pageImage2.hidden = true
            pageImage3.hidden = true
        }
        
        pageControl.numberOfPages = i
        
        
        scrollView = UIScrollView(frame: CGRect(x: 32.0, y: 72.0, width: 245.0, height: 245.0))
        scrollView.pagingEnabled = true
        scrollView.delegate = self
        view.addSubview(scrollView)
            
        let frameScroll = scrollView.frame
        
        
        scrollView.contentSize = CGSize(width: Int(frameScroll.width) * i, height: Int(frameScroll.size.height))
        
        userImage.frame = CGRect(x: 0,y: 0,width: Int(frameScroll.width),height: Int(frameScroll.size.height))
        userImage2.frame = CGRect(x: Int(frameScroll.width),y: 0,width: Int(frameScroll.width),height: Int(frameScroll.size.height))
        userImage3.frame = CGRect(x: Int(frameScroll.width)*2,y: 0,width: Int(frameScroll.width),height: Int(frameScroll.size.height))
        
        scrollView.addSubview(userImage)
        scrollView.addSubview(userImage2)
        scrollView.addSubview(userImage3)
        
        name.frame = CGRectMake(frameX+20, infobarY + 5 ,170, 21)
        name.layer.zPosition = 2
        
        numberPhotos.frame = CGRectMake(215, infobarY + 5 ,26, 21)
    }
    
    func loadData(){
        if (addresseeUser.valueForKey("profilePicture") != nil){
            var image:PFFile = addresseeUser.valueForKey("profilePicture") as! PFFile
             image.getDataInBackgroundWithBlock({ (imageData, error) -> Void in
                if let imageData = imageData {
                    let image = UIImage(data:imageData)
                    //self.photo = image!
                    println("profile")
                    self.photos.addObject(image!)
                    self.updateTime()
                }
            })
           
        }
       
        if (addresseeUser.valueForKey("pictureLeft") != nil) {
            var imageLeft:PFFile = addresseeUser.valueForKey("pictureLeft") as! PFFile
            imageLeft.getDataInBackgroundWithBlock({ (imageDataLeft, error) -> Void in
                if let imageDataLeft = imageDataLeft {
                    let image = UIImage(data:imageDataLeft)
                    //self.photo = image!
                    println("left")
                    self.photos.addObject(image!)
                    self.updateTime()
                }
            })
            
        }
        
        if (addresseeUser.valueForKey("pictureRight") != nil) {
            
            var imageRight:PFFile = addresseeUser.valueForKey("pictureRight") as! PFFile
            imageRight.getDataInBackgroundWithBlock({ (imageDataRight, error) -> Void in
                if let imageDataRight = imageDataRight {
                    let image = UIImage(data:imageDataRight)
                    //self.photo = image!
                    println("right")
                    self.photos.addObject(image!)
                    self.updateTime()
                }
            })
            
        }
        
        let userName:String = (self.addresseeUser.valueForKey("username") as? String)!
        let birthday = self.addresseeUser.valueForKey("birthday") as? NSDate
        let age = birthday != nil ? birthday!.age : 0
        name.text = "\(userName), \(age)"
        
        var photos = 0
        if (addresseeUser.valueForKey("profilePicture") != nil) {
            photos += 1
        }
        if (addresseeUser.valueForKey("pictureLeft") != nil) {
            photos += 1
        }
        if (addresseeUser.valueForKey("pictureRight") != nil) {
            photos += 1
        }
        numberPhotos.text = "\(photos)"
        
        
        var userLoaction:PFGeoPoint = user["location"] as! PFGeoPoint
        var addresseeLoaction:PFGeoPoint = addresseeUser["location"] as! PFGeoPoint
        descriptionLbl.text = self.addresseeUser.valueForKey("description_info") as? String
        let distance = String(format: "%.2f",
            userLoaction.distanceInKilometersTo(addresseeLoaction))
        distanceLbl.text = "A " + distance + "Km. "
        
         var updateTimer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: "updateTime", userInfo: nil, repeats: true)
        
    }
    
    func updateTime(){
        if ourFriendship != nil {
            if let dateAcepted = ourFriendship.valueForKey("acceptedAt") as? NSDate{
                time.text = getTimeDown(NSDate(), dateAcepted, PFUser.currentUser()?.valueForKey("isPlus") as! Bool)
                self.loadImage(getTimeDownSize(NSDate(), dateAcepted, PFUser.currentUser()?.valueForKey("isPlus") as! Bool))
            }else{
                setDefault()
            }
        }else{
            setDefault()
        }
    }
    func setDefault(){
        time.text = "24:00"
        self.loadImage(10)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    func loadImage(){
        self.loadImage(10)
    }
    func loadImage(size:CGFloat){
        for var i = 0; i < photos.count; i++ {
            let photo = photos.objectAtIndex(i) as! UIImage
            
            let width = photo.size.width
            let height = photo.size.height
            let ratio = photo.size.width/photo.size.height
            var newImgMin = resizeImage(photo, CGSize(width: size, height: size / ratio))
            var newImg:UIImage? = resizeImage(newImgMin, CGSize(width: width, height: height))
            
            
            if i == 0 {
                userImage.image = newImg
            } else if i == 1 {
                userImage2.image = newImg
            } else if i == 2 {
                userImage3.image = newImg
            }
        }
    }
    private struct Storyboard{
        static let CellIdentifier = "Friend Cell"
    }
    
    //MARK: scroll view delegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == self.scrollView{
            let pageCurrent : Int = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = pageCurrent
            if pageCurrent == 0 {
                pageImage1.image = UIImage(named: "slide-on")
                pageImage2.image = UIImage(named: "slide-off")
                pageImage3.image = UIImage(named: "slide-off")
            } else if pageCurrent == 1 {
                pageImage1.image = UIImage(named: "slide-off")
                pageImage2.image = UIImage(named: "slide-on")
                pageImage3.image = UIImage(named: "slide-off")
            } else if pageCurrent == 2 {
                pageImage1.image = UIImage(named: "slide-off")
                pageImage2.image = UIImage(named: "slide-off")
                pageImage3.image = UIImage(named: "slide-on")
            }
        }
    }
    
}

extension InformationViewController : UICollectionViewDataSource{

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        lblnoAmigos.hidden = false
        if friendsFacebook.count > 0 {
            lblnoAmigos.hidden = true
            return friendsFacebook.count
            
        }
        return 0
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier(Storyboard.CellIdentifier, forIndexPath: indexPath) as! FriendsCollectionViewCell

        let dic = friendsFacebook.objectAtIndex(indexPath.row) as! NSDictionary
        //cell.user = self.friendsArr[indexPath.item]
        cell.userName.text = dic.objectForKey("first_name") as? String
        cell.userImage.image = UIImage()
        
        var spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        spinner.center = CGPointMake(cell.userImage.frame.size.width/2.0, cell.userImage.frame.size.height/2.0)
        spinner.color = UIColor.blackColor()
        cell.userImage.addSubview(spinner)
        spinner.startAnimating()
        
        var downloadQueue :dispatch_queue_t = dispatch_queue_create("getImagesFriend", nil)
        dispatch_async(downloadQueue, {
            let url = NSURL(string: dic.objectForKey("picture")?.objectForKey("data")!.objectForKey("url") as! String)
            let dataImg = NSData(contentsOfURL: url!)
            dispatch_async(dispatch_get_main_queue(), {
                spinner.stopAnimating()
                if let dataImg = dataImg {
                    cell.userImage.image = UIImage(data: dataImg)
                }
            })
        })

        
        return cell
    }
}








