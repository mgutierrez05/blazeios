//
//  NoUserItemViewController.swift
//  blaze
//
//  Created by devmg on 13/7/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import UIKit

class NoUserItemViewController: UIViewController {
    
    @IBOutlet weak var reloadbtn: UIButton!
    
    override func viewDidLoad() {
    }
    
    @IBAction func reloadUser(sender: AnyObject) {
        self.performSegueWithIdentifier("goToLoading2", sender: self)
    }
}
