//
//  UILabelExtension.swift
//  blaze
//
//  Created by devmg on 24/8/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import Foundation

extension UILabel {
    func sizeOfString (width: CGFloat) -> CGSize{
        let attributes = [NSFontAttributeName : font]
        numberOfLines = 0
        lineBreakMode = NSLineBreakMode.ByWordWrapping
//        let rect = text!.boundingRectWithSize(CGSizeMake(width, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil)
        sizeToFit()
        return frame.size
       // return rect.size
    }
}