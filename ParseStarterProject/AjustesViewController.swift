//
//  AjustesViewController.swift
//  blaze
//
//  Created by Armando Trujillo Zazueta on 6/10/15.
//  Copyright (c) 2015 Armando Trujillo Zazueta. All rights reserved.
//

import UIKit
import Parse
import Foundation

class AjustesViewController: UIViewController, UIAlertViewDelegate {
    
    var mensaje = true
    var vibraciones = true
    var preferencias:PFObject!
    
    @IBOutlet weak var textViewPoliticas: UITextView!
    @IBOutlet weak var webViewPoliticas: UIWebView!
    @IBOutlet weak var btnMensajesActivado: UIButton!
    @IBOutlet weak var btnMensajesDesactivado: UIButton!
    @IBOutlet weak var btnVibracionesActivado: UIButton!
    @IBOutlet weak var btnVibracionesDesactivado: UIButton!
    
    @IBOutlet weak var btnPrivacidad: UIButton!
    @IBOutlet weak var btnCerrar: UIButton!
    @IBOutlet weak var btnEliminar: UIButton!
    @IBOutlet weak var viewMenu : UIView!
    
    override func viewDidLoad() {
        var pdfLoc = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("politicas", ofType:"pdf")!)
        var request = NSURLRequest(URL: pdfLoc!);
        webViewPoliticas.loadRequest(request);
        webViewPoliticas.hidden = true
        textViewPoliticas.hidden = true
        loadPreferencias()
        loadToolBar()
    }
    
    override func viewDidLayoutSubviews() {
        if UIScreen.mainScreen().bounds.size.height == 480 {
            //downloandBtn.frame.size.height = 100;
            let heightConstraintPrivacidad = NSLayoutConstraint(item: btnPrivacidad, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 50)
            view.addConstraint(heightConstraintPrivacidad)
            let heightConstraintCerrar = NSLayoutConstraint(item: btnPrivacidad, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 50)
            view.addConstraint(heightConstraintCerrar)
            let heightConstraintEliminar = NSLayoutConstraint(item: btnPrivacidad, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 50)
            view.addConstraint(heightConstraintEliminar)
            let heightConstraintViewMenu = NSLayoutConstraint(item: viewMenu, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 50)
            view.addConstraint(heightConstraintViewMenu)
            
        } else {
            let heightConstraintPrivacidad = NSLayoutConstraint(item: btnPrivacidad, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 65)
            view.addConstraint(heightConstraintPrivacidad)
            let heightConstraintCerrar = NSLayoutConstraint(item: btnPrivacidad, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 65)
            view.addConstraint(heightConstraintCerrar)
            let heightConstraintEliminar = NSLayoutConstraint(item: btnPrivacidad, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 65)
            view.addConstraint(heightConstraintEliminar)        }
    }
    
    func loadToolBar(){
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationItem.hidesBackButton = true
        
        self.navigationController?.navigationBar.frame.origin.y = 35
        self.navigationController!.navigationBar.clipsToBounds = true;
    }
    
    func loadPreferencias() {
        let innerP1 = NSPredicate(format: "userId = %@", PFUser.currentUser()?.valueForKey("objectId") as! String)
        var query = PFQuery(className: "preferences", predicate: innerP1)
        
        query.addAscendingOrder("createdAt")
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if var objs = objects as? [PFObject] {
                if objs.count > 0 {
                    self.preferencias = objs[0]
                    
                    if self.preferencias.valueForKey("messages") as! Bool {
                        self.btnMensajesActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
                        self.btnMensajesDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                    } else {
                        self.btnMensajesActivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                        self.btnMensajesDesactivado.setBackgroundImage(UIImage(named: "red-circle"), forState: UIControlState.Normal)
                    }
                    
                    if self.preferencias.valueForKey("vibraciones") as! Bool {
                        self.btnVibracionesActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
                        self.btnVibracionesDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                    } else {
                        self.btnVibracionesActivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                        self.btnVibracionesDesactivado.setBackgroundImage(UIImage(named: "red-circle"), forState: UIControlState.Normal)
                    }
                }else{
                    var myPreferences:PFObject = PFObject(className: "preferences")
                    var groupACL = PFACL()
                    // 1 Hombres
                    // 2 Mujeres
                    var genderNumber = 1
                    var genderString = "male"
                    
                    if PFUser.currentUser()?.valueForKey("gender") as! String == "male" {
                        genderNumber = 2
                        genderString = "female"
                    }
                    
                    myPreferences["userId"] = PFUser.currentUser()?.valueForKey("objectId")
                    myPreferences["found_me"] = true
                    myPreferences["distance"] = 2.5
                    myPreferences["age_min"] = 18
                    myPreferences["age_max"] = 55
                    myPreferences["show_gender"] = genderNumber
                    myPreferences["messages"] = true
                    myPreferences["vibraciones"] = true
                    
                    self.preferencias = myPreferences
                    NSUserDefaults.standardUserDefaults().setValue(2.5, forKey: "distance")
                    NSUserDefaults.standardUserDefaults().setValue(genderString, forKey: "genderSearch")
                    NSUserDefaults.standardUserDefaults().setValue(18, forKey: "age_min")
                    NSUserDefaults.standardUserDefaults().setValue(55, forKey: "age_max")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    myPreferences.saveInBackground()
                    
                    self.btnMensajesActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
                    self.btnMensajesDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                    
                    self.btnVibracionesActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
                    self.btnVibracionesDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
                }
            }
        }
    }

    
    @IBAction func goToBack(botton: UIButton) {
        //labelTitle.removeFromSuperview()
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func cerrarSesion() {
        let alert = UIAlertView(title: NSLocalizedString("Saliendo de Blaze", comment: ""), message: NSLocalizedString("¿Esta seguro que quiere cerrar session?", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("No", comment: ""), otherButtonTitles: NSLocalizedString("Si", comment: ""))
        alert.tag = 16
        alert.show()
    }
    
    @IBAction func actionChangeMensaje(botton: UIButton){
        if botton.tag == 1 {
            preferencias["messages"] = true
            btnMensajesActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
            btnMensajesDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        } else if botton.tag == 2 {
            preferencias["messages"] = false
            btnMensajesActivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
            btnMensajesDesactivado.setBackgroundImage(UIImage(named: "red-circle"), forState: UIControlState.Normal)
        }
        preferencias.saveInBackground()
    }
    
    @IBAction func actionChangeVibraciones(botton: UIButton){
        if botton.tag == 4 {
            preferencias["vibraciones"] = true
            btnVibracionesActivado.setBackgroundImage(UIImage(named: "green-circle"), forState: UIControlState.Normal)
            btnVibracionesDesactivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
        } else if botton.tag == 3 {
            preferencias["vibraciones"] = false
            btnVibracionesActivado.setBackgroundImage(UIImage(named: "gray-circle"), forState: UIControlState.Normal)
            btnVibracionesDesactivado.setBackgroundImage(UIImage(named: "red-circle"), forState: UIControlState.Normal)
        }
        preferencias.saveInBackground()
    }
    
    @IBAction func actionShowPoliticas(button: UIButton){
        textViewPoliticas.hidden = !textViewPoliticas.hidden
        webViewPoliticas.hidden = !webViewPoliticas.hidden
    }
    
    @IBAction func deleteAccount(){
        let alert = UIAlertView(title:  NSLocalizedString("Eliminando Cuenta", comment: ""), message:  NSLocalizedString("¿Esta seguro que quiere eliminar su cuenta?", comment: ""), delegate: self, cancelButtonTitle:  NSLocalizedString("No", comment: ""), otherButtonTitles:  NSLocalizedString("Si", comment: ""))
        alert.tag = 15
        alert.show()
    }
    
    //MARK: UIAlertView - delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 15 {
            if buttonIndex == 1 {
                println("borrando...")
                //let innerP1 = NSPredicate(format: "toUser = %@", "Kslk8mYf6h")
                let innerP1 = NSPredicate(format: "fromUser = %@ ",PFUser.currentUser()!)
                var innerQ1 = PFQuery(className: "friendship", predicate: innerP1)
                
                let innerP2 = NSPredicate(format: "toUser = %@",PFUser.currentUser()!)
                var innerQ2 = PFQuery(className: "friendship", predicate: innerP2)
                
                var query = PFQuery.orQueryWithSubqueries([innerQ1,innerQ2])
                query.addAscendingOrder("updatedAt")
                query.findObjectsInBackgroundWithBlock {
                    (objectsFriendShip: [AnyObject]?, error: NSError?) -> Void in
                    if error == nil {
                        if let obsFriend = objectsFriendShip as? [PFObject] {
                            if obsFriend.count > 0 {
                                
                                var arrayQuerys = [PFQuery]()
                                
                                for obsFriendFor in obsFriend {
                                    let innerP1 = NSPredicate(format: "friendship = %@ ", obsFriendFor)
                                    var innerQ1 = PFQuery(className: "Messages", predicate: innerP1)
                                    arrayQuerys.append(innerQ1)
                                }
                                
                                var queryMessage = PFQuery.orQueryWithSubqueries(arrayQuerys)
                                queryMessage.findObjectsInBackgroundWithBlock {
                                    (objects: [AnyObject]?, error: NSError?) -> Void in
                                    if error == nil {
                                        if let objects = objects as? [PFObject] {
                                            if objects.count > 0 {
                                                println("borrando mensajes")
                                                PFObject.deleteAllInBackground(objects, block: {
                                                    (success: Bool, error: NSError?) -> Void in
                                                    if (success) {
                                                        PFObject.deleteAllInBackground(objectsFriendShip, block: {
                                                            (success: Bool, error: NSError?) -> Void in
                                                            if (success) {
                                                                self.eliminarUsuario()
                                                            } else {
                                                                // There was a problem, check error.description
                                                                println("ocurrio un error al elimnar")
                                                            }
                                                        })
                                                    } else {
                                                        println("ocurrio un error al elimnar")
                                                    }
                                                })
                                            } else {
                                                PFObject.deleteAllInBackground(objectsFriendShip, block: {
                                                    (success: Bool, error: NSError?) -> Void in
                                                    if (success) {
                                                        self.eliminarUsuario()
                                                    } else {
                                                        // There was a problem, check error.description
                                                        println("ocurrio un error al elimnar")
                                                    }
                                                })
                                            }
                                        }
                                    }
                                }
                            }   else {
                                self.eliminarUsuario()
                            }
                        }
                    }
                }
            }
        } else if alertView.tag == 16 {
            if buttonIndex == 1 {
                PFFacebookUtils.session()?.closeAndClearTokenInformation()
                PFUser.logOut()
                navigationController?.setNavigationBarHidden(true, animated: true)
                self.navigationController!.popToRootViewControllerAnimated(false)
            }
        }
    }
    
    func eliminarUsuario() {
        println("eliminando")
        preferencias.delete()
        PFUser.currentUser()?.deleteInBackground()
        PFUser.logOut()
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController!.popToRootViewControllerAnimated(false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        //        if (segue.identifier == "goToUserList") {
        //            var svc = segue.destinationViewController as! UserListViewController;
        //            svc.arrayInformationMessages = self.arrayInformationMessages
        //
        //        }
        //        if (segue.identifier == "dimmisLoading") {
        //            var svc = segue.destinationViewController as! UserListViewController;
        //            svc.arrayInformationMessages = self.arrayInformationMessages
        //            
        //        }
    }
}