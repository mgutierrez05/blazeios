//
//  ScrollControlProtocol.swift
//  blaze
//
//  Created by devmg on 11/8/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

protocol ScrollControlProtocol {
    func disableScroll()->Void
    func enableScroll()->Void
}