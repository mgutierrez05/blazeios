//
//  Helper.swift
//  blaze
//
//  Created by devmg on 25/8/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import Foundation
func getTimeDown(fromDate:NSDate, toDate:NSDate, isPlus: Bool)->String{
    var formatter = NSDateFormatter()
    formatter.dateFormat = "HH:mm"
    let periodComponents = NSDateComponents()

    if  isPlus {
        periodComponents.hour = +1
    } else {
        periodComponents.hour = +24
    }
    
    let userCalendar = NSCalendar.currentCalendar()
    let date3 = userCalendar.dateByAddingComponents(
        periodComponents,
        toDate: toDate,
        options: nil)!
    println(date3.datesFrom(fromDate))
    println(date3.hoursFrom(fromDate))
    var dateString = ""
    println(date3.minutesFrom(fromDate))
    if date3.hoursFrom(fromDate) >= 0 && date3.minutesFrom(fromDate) > 0 {
        dateString = formatter.stringFromDate(date3.datesFrom(fromDate))
    }else{
        dateString = "00:00"
    }
    return dateString
}
func getTimeDownSize(fromDate:NSDate, toDate:NSDate, isPlus: Bool)->CGFloat{
    var formatter = NSDateFormatter()
    formatter.dateFormat = "HH:mm"
    let periodComponents = NSDateComponents()
    if  isPlus {
        periodComponents.hour = +1
    } else {
        periodComponents.hour = +24
    }
    let userCalendar = NSCalendar.currentCalendar()
    let date3 = userCalendar.dateByAddingComponents(
        periodComponents,
        toDate: toDate,
        options: nil)!
    println(date3.datesFrom(fromDate))
    println(date3.hoursFrom(fromDate))
    var size:CGFloat = 10
    if date3.hoursFrom(fromDate) <= 0 && date3.minutesFrom(fromDate) <= 0 {
        size = 200
    }
    if date3.hoursFrom(fromDate) > 0 {
        size = 30
    }
    if date3.hoursFrom(fromDate) > 6 {
        size = 20
    }
    if date3.hoursFrom(fromDate) > 12 {
        size = 15
    }
    println(size)
    return size
}