//
//  LocationPrimise.swift
//  blaze
//
//  Created by devmg on 11/7/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//


import Foundation

protocol LocationFinishable {
    func onFail(fail: ((error: NSError) -> ())) -> LocationFinishable
    func onSuccess(done: ((location: CLLocation) -> ()))  -> LocationFinishable
}

class LocationPromise : LocationFinishable {
    
    // A callback to call when we're completely done.
    var done: ((location: CLLocation) -> ()) = {location in }
    
    // A callback to invoke in the event of failure.
    var fail: ((error: NSError) -> ()) = {error in }

    
    // Class ("static") method to return a new promise.
    class func defer() -> LocationPromise {
        return LocationPromise()
    }
    
 
    
    // Fail method.
    func onFail(fail: ((error: NSError) -> ())) -> LocationFinishable {
        self.fail = fail
        let finishablePromise : LocationFinishable = self
        return finishablePromise
    }
    
    // Done method.
    func onSuccess(done: ((location: CLLocation) -> ()))  -> LocationFinishable{
        self.done = done
        let finishablePromise : LocationFinishable = self
        return finishablePromise
    }
}