//
//  LoadingViewController.swift
//  blaze
//
//  Created by devmg on 6/7/15.
//  Copyright (c) 2015 DevMg. All rights reserved.
//

import UIKit
import Parse
import Foundation

class LoadingViewController: UIViewController {
    
    @IBOutlet weak var loading: UIImageView!
    var arrayInformationMessages:[PFObject] = []
    let MAX_DISTANCE = 5.0
    
    var isModal:Bool = false
    
    weak var timer: NSTimer?
    override func viewDidLoad() {
        startAnim()
//        if !self.isModal {
//            startAnim()
//        }
    }
    
    func startAnim(){
    
        loading.animationImages = [UIImage]()
        for var index = 1; index < 5; index++ {
            var frameName = "anim\(index)"
            loading.animationImages?.append(UIImage(named: frameName)!)
        }
        loading.animationDuration = 1.5
        loading.startAnimating()
        resetTimer()
        navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: false)
    }
    
    func resetTimer() {
        timer?.invalidate()
        let nextTimer = NSTimer.scheduledTimerWithTimeInterval(2.1, target: self, selector: "handleIdleEvent:", userInfo: nil, repeats: false)
        timer = nextTimer
    }
    
    func handleIdleEvent(timer: NSTimer) {
        loadListUser()
//        self.performSegueWithIdentifier("goToUserList", sender: self)
    }
    
    func dateLessYears(year: Int) -> NSDate {
        
        let formater = NSDateFormatter()
        formater.dateFormat = "yyyy"
        let currentYeary = String(formater.stringFromDate(NSDate()).toInt()! - year)
        formater.dateFormat = "MM"
        let month = formater.stringFromDate(NSDate())
        formater.dateFormat = "dd"
        let day = formater.stringFromDate(NSDate())
        
        formater.dateFormat = "yyyy-MM-dd"
        
        println(formater.dateFromString("\(currentYeary)-\(month)-\(day)"))
        return formater.dateFromString("\(currentYeary)-\(month)-\(day)")!
    }
    
    func loadListUser(){
        var distance = 5.0
        if (NSUserDefaults.standardUserDefaults().valueForKey("distance") != nil) {
            distance = (NSUserDefaults.standardUserDefaults().valueForKey("distance") as? Double)!
        }
        let userObject = PFUser.currentUser()
        userObject?.fetch()
        var blockedUsers = [String]()
        if var blkUsr = userObject!.valueForKey("blockedUsers") as? [String]{
            blockedUsers = blkUsr
        }
        // User's location
        let userGeoPoint:PFGeoPoint = userObject?.valueForKey("location") as! PFGeoPoint
        // Create a query for places
        var query = PFUser.query()
        // Interested in locations near user.notEqualTo
        query!.whereKey("objectId", notEqualTo:userObject!.objectId!)
        query!.whereKey("objectId", notContainedIn: blockedUsers)
        query!.whereKey("blockedUsers", notEqualTo:userObject!.objectId!)
        query!.whereKey("location", nearGeoPoint:userGeoPoint,withinKilometers: distance)
        
        if (NSUserDefaults.standardUserDefaults().valueForKey("genderSearch") != nil) {
            query!.whereKey("gender", equalTo: NSUserDefaults.standardUserDefaults().valueForKey("genderSearch")!)
        }
        
        if (NSUserDefaults.standardUserDefaults().valueForKey("age_min") != nil) {
            let date = dateLessYears(NSUserDefaults.standardUserDefaults().valueForKey("age_min") as! Int)
            query!.whereKey("birthday", lessThan: date)
        }
        
        if (NSUserDefaults.standardUserDefaults().valueForKey("age_max") != nil) {
            let date = dateLessYears(NSUserDefaults.standardUserDefaults().valueForKey("age_max") as! Int)
            query!.whereKey("birthday", greaterThanOrEqualTo: date)
        }
    
        let queryPreferencias = PFQuery(className: "preferences")
        queryPreferencias.whereKey("found_me", equalTo: true)
//      queryPreferencias.findObjects()
        query?.whereKey("objectId", matchesKey: "userId", inQuery: queryPreferencias)
        
        
        query!.limit = 10
        // Final list of objects
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                println("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                self.arrayInformationMessages = (objects as? [PFObject])!
                
                if self.isModal {
                
//                    let switchViewController = self.navigationController?.viewControllers[2] as! UserListViewController
//                    
//                    self.navigationController?.popToViewController(switchViewController, animated: true)
                   
                    self.performSegueWithIdentifier("dimmisLoading", sender: self)
                    
                    
                }
                else{
                    self.performSegueWithIdentifier("goToUserList", sender: self)
                }
               
            } else {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goToUserList") {
            var svc = segue.destinationViewController as! UserListViewController;            
            svc.arrayInformationMessages = self.arrayInformationMessages
            
        }
        if (segue.identifier == "dimmisLoading") {
            var svc = segue.destinationViewController as! UserListViewController;
            svc.arrayInformationMessages = self.arrayInformationMessages
            
        }
    }


}