	// The MIT License (MIT)
//
// Copyright (c) 2015
// King-Wizard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import UIKit
import Parse

class UserItemViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var frame: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var infobar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numberPhotos: UILabel!
    @IBOutlet weak var chatBtn: UIButton!
    
    var index: Int = 0
    var user:PFObject!
    var photo:UIImage!
    var scrollControl:UserListViewController!

    var locationStart:CGFloat = 0
    var locationEnd:CGFloat = 0
    var originalCenter = CGPoint()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUIConfiguration()
        loadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if UIScreen.mainScreen().bounds.size.height == 480 {
            let constraint = NSLayoutConstraint(item: frame, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: frame, attribute: NSLayoutAttribute.Height, multiplier: 346.0/275.0, constant: 0)
            frame.addConstraint(constraint)
            
            println(userImage.frame.height)
            
            let heightConstraint = NSLayoutConstraint(item: chatBtn, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 58)
            view.addConstraint(heightConstraint)
        }
    }
    
 
    
    @IBAction func goToMessage(sender: AnyObject) {
        self.performSegueWithIdentifier("goToMessage", sender: self)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goToMessage") {
            var svc = segue.destinationViewController as! MessagesController;
            svc.addresseeUser = user as! PFUser
            svc.senderUser = PFUser.currentUser()
            
        }
    }
    func loadUIConfiguration(){
        let frameRatio:CGFloat = (254/266)
        let infobarRatio:CGFloat = 0.1364
        let viewWidht = view.frame.size.width
        let viewHeight = view.frame.size.height
        let frameWidth = viewWidht - (viewWidht * (213 / 1242))
        let frameHeight = frameWidth * frameRatio
        let userImageWidth = (932 * viewWidht) / 1242
        frame.frame = CGRectMake(CGFloat(26),CGFloat(100),frameWidth,frameHeight)
        userImage.frame = CGRectMake(CGFloat(32),CGFloat(108),frameWidth-20,frameWidth-25)
//        userImage.layer.zPosition = 2
        userImage.clipsToBounds = true
        userImage.contentMode = .ScaleAspectFill
        let infobarHeight = frameWidth *  infobarRatio
        let infobarY = frameHeight + infobarHeight + 63
        infobar.frame = CGRectMake(CGFloat(26), infobarY, frameWidth, infobarHeight)
        
        name.frame = CGRectMake(40, infobarY + 5 ,170, 21)
        name.layer.zPosition = 2
        
        numberPhotos.frame = CGRectMake(215, infobarY + 5 ,26, 21)
        
    }
    func loadData(){
        if (user.valueForKey("profilePicture") != nil) {
            var image:PFFile = user.valueForKey("profilePicture") as! PFFile
            image.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        let image = UIImage(data:imageData)
                        self.photo = image!
                        self.loadImage()
                    }
                }
            }
        }
        
        let userName:String = (self.user.valueForKey("username") as? String)!
        let birthday = self.user.valueForKey("birthday") as? NSDate
        let age = birthday != nil ? birthday!.age : 0
        name.text = "\(userName), \(age)"
        
        var photos = 0
        if (user.valueForKey("profilePicture") != nil) {
            photos += 1
        }
        if (user.valueForKey("pictureLeft") != nil) {
            photos += 1
        }
        if (user.valueForKey("pictureRight") != nil) {
            photos += 1
        }
        numberPhotos.text = "\(photos)"
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    func loadImage(){
        
        let width = photo.size.width
        let height = photo.size.height
        let ratio = photo.size.width/photo.size.height
        var newImgMin = resizeImage(photo, CGSize(width: 10, height: 10 / ratio))
        var newImg:UIImage? = resizeImage(newImgMin, CGSize(width: width, height: height))
        userImage.image = newImg
        
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
                println("start")
                if let touch =  touches.first as? UITouch {
                    originalCenter.x = touch.locationInView(self.view).x
                    originalCenter.y = touch.locationInView(self.view).y
                    scrollControl.disableScroll()
//                    self.scrollControl.transitionManager.began()
                }
        
            }
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
                if let touch =  touches.first as? UITouch {
                    println("first")
                    let rootlocation = touch.locationInView(self.parentViewController?.view)
                    let location = touch.locationInView(self.view)
                    let previousLocation = touch.previousLocationInView(self.view)
                    var dif = location.y - previousLocation.y
                    var deltaY =  rootlocation.y - previousLocation.y
//                    self.scrollControl.transitionManager.change(CGFloat(deltaY/400))
                    self.scrollControl.showLogoAnim(CGFloat(deltaY/95))
                    println(dif)
//                     println(dif)
                    if dif > 0 && deltaY < 95{
                        view.center.y += location.y - previousLocation.y
                    }
                    if dif < 0  && self.view.frame.origin.y > 0{
                        view.center.y += location.y - previousLocation.y
                    }
                }else{
                    println("second")
                }
            }
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
                
        
        
//                self.scrollControl.transitionManager.ended()
        
                if let touch =  touches.first as? UITouch {
                    let rootlocation = touch.locationInView(self.parentViewController?.view)
                    let previousLocation = touch.previousLocationInView(self.view)
                 
                    var deltaY =  rootlocation.y - previousLocation.y
                    if deltaY > 93{
                         self.scrollControl.goToLoadingAnim()
                    }else{
                        self.scrollControl.fadeOutLogoAnim()
                        UIView.animateWithDuration(0.25, animations: {
                            self.view.frame.origin.y = 0
                            
                        })
                    }
                   scrollControl.enableScroll()
                    println("end")
                }
                
            }
    override func touchesCancelled(touches: Set<NSObject>, withEvent event: UIEvent) {
         println("canceled")
        scrollControl.enableScroll()
//            view.frame.origin.y = 20
        
    }
   
    
}
