//
//  AlbumViewController.swift
//  Blaze
//
//  Created by Armando Trujillo Zazueta  on 12/10/15.
//  Copyright © 2015 Armando Trujillo zazueta. All rights reserved.
//

import UIKit
import Parse

class AlbumViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView : UITableView!
    private var arrayAlbums : NSMutableArray = NSMutableArray()
    private var dicPhotos = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var completionHandler = { connection, result, error in
            //  println(result)
            println(error)
            if result != nil {
                self.arrayAlbums = result["data"] as! NSMutableArray
                self.tableView.reloadData()
            }
        
            } as FBRequestHandler;
        var parameterts = ["fields" : "photos{images,width},name"]
        FBRequestConnection.startWithGraphPath("me/albums", parameters: parameterts, HTTPMethod: "GET", completionHandler: completionHandler)
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayAlbums.count > 0 {
            return arrayAlbums.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ImageLabelTableCell") as! ImageLabelTableCell
        let dic = arrayAlbums.objectAtIndex(indexPath.row) as! NSDictionary
        println(dic)

        cell.imageGallery.image = UIImage(named: "noImage")
        
        if let photos = dic.objectForKey("photos") as? NSDictionary {
            if let data = photos.objectForKey("data") as? NSArray{
                if let dataItem = data.objectAtIndex(0) as? NSDictionary {
                    if let images = dataItem.objectForKey("images") as? NSArray {
                        if let item = images.objectAtIndex(0) as? NSDictionary {
                            if let urlString = item.objectForKey("source") as? String {
                                if let imgURL: NSURL = NSURL(string: urlString) {
                                    if let url = NSURL(string: urlString) {
                                        if let image = dicPhotos.objectForKey(url) as? UIImage {
                                            cell.imageGallery.image = image
                                        } else {
                                            downloadImage(url, imageView: cell.imageGallery)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
                 cell.imageGallery.image = UIImage(named: "noImage")
        }
        
        cell.label.text = dic.valueForKey("name") as? String
        
        return cell
    }
    var indexRow = -1
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let dic = arrayAlbums.objectAtIndex(indexPath.row) as! NSDictionary
        if let photos = dic.objectForKey("photos") as? NSDictionary {
            if let data = photos.objectForKey("data") as? NSArray{
                if let dataItem = data.objectAtIndex(0) as? NSDictionary {
                    if let images = dataItem.objectForKey("images") as? NSArray {
                        if let item = images.objectAtIndex(0) as? NSDictionary {
                            if let urlString = item.objectForKey("source") as? String {
                                if let imgURL: NSURL = NSURL(string: urlString) {
                                    if let url = NSURL(string: urlString) {
                                        indexRow = indexPath.row
                                        self.performSegueWithIdentifier("pushGallery", sender: self)
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        UIAlertView(title: "", message: NSLocalizedString("Este Album no contiene fotos", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Aceptar", comment: "")).show()
    }
    
    func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }
    func downloadImage(url: NSURL, imageView: UIImageView){
        print("Started downloading \"\(url.URLByDeletingPathExtension!.lastPathComponent!)\".")
        getDataFromUrl(url) { (data, response, error)  in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                //guard; let data = data; where error == nil else { return }
                print("Finished downloading \"\(url.URLByDeletingPathExtension!.lastPathComponent!)\".")
                imageView.image = UIImage(data: data!)
                self.dicPhotos.setObject(imageView.image!, forKey: url)
            }
        }
    }
    
    //MARK: Segue 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if ( segue.identifier == "pushGallery" && indexRow >= 0) {
            let destination = segue.destinationViewController as! GalleryViewController
            let dic = arrayAlbums.objectAtIndex(indexRow) as! NSDictionary
            
            destination.navigationItem.title = dic.objectForKey("name") as! String
            if let photos = dic.objectForKey("photos") as? NSDictionary {
                let data = photos.objectForKey("data") as! NSMutableArray
                destination.array = data
            }
        }
    }
    
    
    
    //MARK: Actions Buttons
    @IBAction func actionCancel(button: AnyObject){
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
}
